#!/usr/bin/env bash

set -x

export component=$1
export git_branch=$2
export git_url=$3

echo "Create /tmp/artifacts if not already there"
mkdir /tmp/artifacts

echo "Create /mnt/tests as beaker task location"
mkdir /mnt/tests

# setup_repos.py rawhide 2>&1 | tee /tmp/artifacts/setup_repos.txt

# tar -xvzf lister.tar.gz -C /tmp/artifacts

# install stuff for lister even not sure we can always access the system
dnf -y install php screen git yum initscripts --skip-broken

# copy lister to artifacts
cp -r lister/* /tmp/artifacts

# run initial setup
./initial_setup.sh 2>&1 | tee /tmp/artifacts/initial_setup.txt

rm ./beaker.xml
rm /mnt/beaker.xml
# make xml for the os_job_runner to follow. Rationale in os_job_prepare.py
./os_job_prepare.py ${component} ${git_branch} ${git_url} 2>&1 | tee /tmp/artifacts/job_prepare.txt

cp ./beaker.xml /mnt/
if [ $? -ne 0 ]; then
  echo "Failed to generate run XML based on mapper.yaml repo metadata"
fi
# run the 'beaker' xml task runner
./os_job_runner.py 2>&1 | tee /tmp/artifacts/runner.txt

# list /tmp/artifacts so we have a list of logs on main log
ls -la /tmp/artifacts

# cat junit so we have a detail of results on main log
cat /tmp/artifacts/junit.xml

exit $(cat /tmp/artifacts/harness_rc.txt)
