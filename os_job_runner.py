#!/usr/bin/env python

import sys
import os
import logging
import subprocess

try:
    from beaker_xml import BeakerXml
except Exception:
    from beaker.beaker_xml import BeakerXml

beaker_xml_path = '/mnt/beaker.xml'
log_dir = '/tmp/artifacts/'
overview_path = '/tmp/artifacts/overview.txt'

print(os.getcwd())


class TaskRunner(object):

    skip_tasks = ['/distribution/install']

    def __init__(self, xml_path):
        logging.basicConfig(format='%(asctime)s >>> %(message)s', datefmt='%H:%M:%S')
        self.log = logging.getLogger('runner')
        self.log.setLevel('INFO')

        self.log.info('Loading the Beaker XML')
        with open(xml_path, 'r') as f:
            self.xml = f.read()
        self.log.info(self.xml)
        self.beaker_xml = BeakerXml(beaker_xml_path)
        self.task_list = self.beaker_xml.get_task_list()
        os.system("echo '%s' > %s" % (self.beaker_xml.whiteboard, overview_path))
        if 'selinux=0' in self.beaker_xml.kernel_options_post:
            self.log.info('Per XML settings putting SELINUX to PERMISSIVE mode')
            os.system("setenforce 0")
        self._index = 0

    def run_all_tasks(self):
        while self._index < len(self.task_list):
            self.run_next_task()

    def run_next_task(self):
        task = self.task_list[self._index]
        # import ipdb; ipdb.set_trace()
        self.log.info("\n\n\n\n\n\n\nProcessing task")
        self.log.info('======================== %s =======================' % task[0])
        if task[0] in self.skip_tasks:
            self._index = self._index + 1
            self.log.info('This task is tagged to get skipped')
            return
        if task[0] == '/distribution/pkginstall':
            task[0] = '/distribution/command'
            try:
                pkgargname = task[1]['PKGARGNAME']
            except:
                self.log.error("PKGARGNAME missing!!! Cannot rewrite to /distribution/command")
                self._index = self._index + 1
                return 1
            task[1] = {'CMDS_TO_RUN' : 'yum -y install %s' % pkgargname}
        orig_dir = os.getcwd()
        self.log.info('Coming to task directory')
        os.chdir("/mnt/tests%s" % task[0])
        self.log.info("Injecting environ")
        environ = os.environ.copy()
        print(task)
        for param in task[1].keys():
            environ[param] = task[1][param]
        self.log.info("Executing task")
        task_logname = 'out_' + ('t%d' % self._index) + task[0].replace('/','_')
        retval = subprocess.Popen('./runtest.sh 2>&1 | tee /tmp/artifacts/%s.txt ; exit ${PIPESTATUS[0]}' % task_logname, env=environ, shell=True).wait()
        self.log.info("Task completed wth RC=%d" % retval)
        os.chdir(orig_dir)
        self._index = self._index + 1
        return retval


def run():
    os.system('echo "" > %s' % overview_path)
    runner = TaskRunner(beaker_xml_path)
    runner.run_all_tasks()


if __name__ == "__main__":
    run()
