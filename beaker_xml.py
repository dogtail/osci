import os
import sys
import xml.etree.ElementTree as ET

class BeakerXml(object):

    def __init__(self, template):
        self._xml = ET.parse(template)
        self._job = self._xml.getroot()
        self._recipe_set = self._job.find('recipeSet')
        assert self._recipe_set is not None, "Did not find the recipe set!"
        self._recipe = self._recipe_set.find('recipe')
        assert self._recipe is not None, "Did not find the recipe!"
        self._distroRequires = self._recipe.find('distroRequires')
        assert self._distroRequires is not None, "Did not find the distroRequires!"
        self._hostRequires = self._recipe.find('hostRequires')
        assert self._distroRequires is not None, "Did not find the hostRequires!"
        self._task_list = self._recipe.findall('task')

    def parse_beaker_template(self, template):
        self.__init__(template)

    def append_task(self, task_name, task_params=None):
        '''Create and append task to the recipe'''
        task = self._contruct_task(task_name, task_params)
        index = self._get_recipe_child_index(self._task_list[-1]) + 1
        self._recipe.insert(index, task)
        self._task_list = self._recipe.findall('task')

    def insert_task(self, task_name, task_params, position):
        '''Insert task on a given position. 0 being position of a first existing task'''
        task = self._contruct_task(task_name, task_params)
        index = self._get_recipe_child_index(self._task_list[position])
        self._recipe.insert(index, task)
        self._task_list = self._recipe.findall('task')

    def insert_task_before(self, task_name, task_params, before):
        '''Insert task before a first existing task of given name'''
        task = self._contruct_task(task_name, task_params)
        ex_task = [x for x in self._task_list if before in x.get('name')][0]
        index = self._get_recipe_child_index(ex_task)
        self._recipe.insert(index, task)
        self._task_list = self._recipe.findall('task')

    def insert_task_after(self, task_name, task_params, after):
        '''Insert task after a last existing task of given name'''
        task = self._contruct_task(task_name, task_params)
        ex_task = [x for x in self._task_list if after in x.get('name')][-1]
        index = self._get_recipe_child_index(ex_task) + 1
        self._recipe.insert(index, task)
        self._task_list = self._recipe.findall('task')

    def set_task_parameter(self, task_name, param_name, param_value):
        '''Set a parameter of existing task'''
        task = [x for x in self._task_list if task_name in x.get('name')][0]
        params = task.find('params')
        if params is None:
            params = ET.SubElement(task, 'params')
        param = [x for x in params.findall('param') if param_name in x.get('name')]
        if len(param) > 0:
            param = param[0]
        else:
            param = ET.SubElement(params, 'param')
        param.set('name', param_name)
        param.set('value', param_value)

    def get_task_parameter(self, task_name, param_name):
        '''Set a parameter of existing task'''
        task = [x for x in self._task_list if task_name in x.get('name')][0]
        params = task.find('params')
        if params is None:
            return None
        param = [x for x in params.findall('param') if param_name in x.get('name')]
        if len(param) > 0:
            return param[0].get('value')
        else:
            return None

    def get_task_list(self):
        '''Get a list of tasks as a pure python tuple (name, param_dict) such as:
        ("/distribution/command", {'CMDS_TO_RUN' : 'yum', 'PARAM2' : 'echo'})'''
        tl = []
        for x in self._task_list:
            name = x.get('name')
            params = {}
            if x.find('params') is not None:
                for p in x.find('params').findall('param'):
                    params[p.get('name')] = p.get('value')
            tl.append([name, params])
        return tl


    def has_tasks(self, task_regex):
        '''Returns list of task names of all tasks matching the regex'''
        import re
        pattern = re.compile(task_regex)
        return [x for x in self._task_list if pattern.match(x.get('name'))]

    def get_task_objects(self, task_name):
        '''Returns list of task objects of all task with given name'''
        return [x for x in self._task_list if task_name in x.get('name')]

    def _get_requires(self, req_element):
        if len(req_element.getchildren()) == 0:
            return None
        else:
            ret_dict = {}
            if req_element.find('and') is not None:
                for item in req_element.find('and').getchildren():
                    ret_dict[item.tag] = item.get('value')
            for item in req_element.getchildren():
                if item.tag == 'and':
                    pass
                else:
                    ret_dict[item.tag] = item.get('value')
            return ret_dict

    def _set_requires(self, set_dict, req_element):
        for item in req_element.getchildren():
            req_element.remove(item)
        joiner = ET.SubElement(req_element, 'and')
        for key in set_dict:
            op = '='
            value = set_dict[key]
            if isinstance(set_dict[key], list):
                op = set_dict[key][0]
                value = set_dict[key][1]
            value_tag = 'value'
            if '=' in value:
                value_tag, value = value.split('=')
            param = ET.SubElement(joiner, key)
            param.set('op', op)
            param.set(value_tag, value)

    @property
    def distro_requires(self):
        return self._get_requires(self._distroRequires)

    @distro_requires.setter
    def distro_requires(self, distro_dict):
        self._set_requires(distro_dict, self._distroRequires)

    @property
    def host_requires(self):
        return self._get_requires(self._hostRequires)

    @host_requires.setter
    def host_requires(self, host_dict):
        self._set_requires(host_dict, self._hostRequires)

    @property
    def whiteboard(self):
        return self._job.find('whiteboard').text

    @whiteboard.setter
    def whiteboard(self, whiteboard_text):
        self._job.find('whiteboard').text = whiteboard_text

    @property
    def reservesys(self):
        r = self._recipe.find('reservesys')
        if r is not None:
            return r.get('duration')
        else:
            return None

    @reservesys.setter
    def reservesys(self, seconds):
        r = self._recipe.find('reservesys')
        if seconds is not None:
            if r is None:
                r = ET.SubElement(self._recipe, 'reservesys')
            r.set('duration', seconds)
        else:
            if r is not None:
                self._recipe.remove(r)

    @property
    def kernel_options(self):
        return self._recipe.get('kernel_options')

    @kernel_options.setter
    def kernel_options(self, text):
        self._recipe.set('kernel_options', text)

    @property
    def kernel_options_post(self):
        return self._recipe.get('kernel_options_post')

    @kernel_options_post.setter
    def kernel_options_post(self, text):
        self._recipe.set('kernel_options_post', text)

    def get_job_string(self):
        '''Returns a current representation of BeakerXml as a XML string'''
        return ET.tostring(self._job, encoding="utf8")

    def write(self, path):
        self._xml.write(path, encoding="utf8")

    def _get_recipe_child_index(self, child):
        return self._recipe.getchildren().index(child)

    def _contruct_task(self, task_name, task_params):
        task = ET.Element('task')
        task.set('name', task_name)
        task.set('role', "STANDALONE")
        if task_params is not None and len(task_params) > 0:
            params = ET.SubElement(task, 'params')
            for key in task_params:
                param = ET.SubElement(params, 'param')
                param.set('name', key)
                param.set('value', task_params[key])
        return task
