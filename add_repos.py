#!/usr/libexec/platform-python

import sys
import argparse
import logging
import platform
import json
import fnmatch
from urllib.request import urlopen

logger = logging.getLogger(__name__)

def _in_filter(item, enable_list):
    return any([ fnmatch.fnmatch(item, enable_item) for enable_item in enable_list ])

def _repo_enabled(item, enable_list, disable_list):
    if enable_list is None and disable_list is None:
        return True
    if disable_list is not None and any([ fnmatch.fnmatch(item, disable_item) for disable_item in disable_list ]):
        return False
    return any([ fnmatch.fnmatch(item, enable_item) for enable_item in enable_list ])

def get_composeinfo(compose_url):
    composeinfo = json.load(urlopen("/".join((compose_url, "metadata", "composeinfo.json"))))
    return composeinfo

def _get_repoinfos(composeinfo, arch, repoid_prefix, enable_list, disable_list):
    for variant, variant_data in composeinfo['payload']['variants'].items():
        if arch not in variant_data['arches']:
            logger.info("Skipping %s since it's not valid for arch: %s", variant, arch)
            continue
        for repo_type, repoid_suffix in {'repository': '', 'debug_repository': '-debuginfo'}.items():
            if repo_type not in variant_data['paths']:
                continue
            key = variant + repoid_suffix
            repo = variant_data['id'] + repoid_suffix
            value = {
                'id' : repoid_prefix + repo,
                'enabled': _repo_enabled(repo, enable_list, disable_list),
                'repopath' : variant_data['paths'][repo_type][arch],
            }
            yield (key, value)

def get_repoinfos(composeinfo, arch, repoid_prefix, enable_list=None, disable_list=None):
    return dict(_get_repoinfos(composeinfo, arch, repoid_prefix, enable_list, disable_list))

def make_repofile(repo_id, baseurl, repo_name=None, enabled=True, gpgcheck=False, skip_if_unavailable=True, dry_run=False):
    repofilename = '/etc/yum.repos.d/%s.repo' % repo_id
    logger.debug('Locals: %s', locals())
    logger.info('Creating repofile "%s"', repofilename)
    if repo_name is None:
        repo_name = repo_id
    template = """
[%(repo_id)s]
name=%(repo_name)s
baseurl=%(baseurl)s
enabled=%(enabled)d
gpgcheck=%(gpgcheck)d
skip_if_unavailable=%(skip_if_unavailable)d
"""
    data = template % locals()
    if dry_run:
        print(data)
        return
    with open(repofilename, 'w', encoding='UTF-8') as repofile:
        repofile.write(data)
    return repofilename

def make_repofiles(repoinfos, compose_url, dry_run=False):
    repos = []
    for num, repoinfo in enumerate(repoinfos.values()):
        logger.debug("Processing repoinfo: %s", repoinfo)
        baseurl = "/".join((compose_url, repoinfo['repopath']))
        repos.append(make_repofile(repoinfo['id'], baseurl, enabled=repoinfo['enabled'], dry_run=dry_run))
    return repos

def make_repofiles_from_compose(compose_url, arch, repoid_prefix, enable_repos, disable_repos, dry_run=False):
    composeinfo = get_composeinfo(compose_url)
    repoinfos = get_repoinfos(composeinfo, arch, repoid_prefix, enable_repos, disable_repos)
    return make_repofiles(repoinfos, compose_url, dry_run=dry_run)

def main(*in_args):
    parser = argparse.ArgumentParser(description='Add relevant repositories for specified compose.')
    parser.add_argument(
        'compose_url',
        help="URL of compose",
    )
    parser.add_argument(
        '-a', '--arch',
        default=platform.machine(),
        help="Set architecture explicitly. Default is current architecture.",
    )
    parser.add_argument(
        '-e', '--enable-repo', dest='enable_repos',
        action='append',
        help="Only specified repositories will be enabled (globbing enabled). By default, all repositories are enabled. Note that enabling repo disabled by --disable-repo won't have effect.",
    )
    parser.add_argument(
        '-d', '--disable-repo', dest='disable_repos',
        action='append',
        help="Specified repositories will be disabled (globbing enabled). By default, all repositories are enabled. Note that repo disabled by --disable-repos won't be enabled by --enable-repo",
    )
    parser.add_argument(
        '-p', '--repoid-prefix',
        default='osci-',
        help="Prefix used in ids of added repositories",
    )
    parser.add_argument(
        '--dry-run',
        action='store_true',
        help="Don't do anything, just pring repofiles content.",
    )
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
    )
    parser.add_argument(
        '--debug',
        action='store_true',
    )
    args = parser.parse_args(in_args)
    # configure logging
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    if args.verbose:
        ch.setLevel(logging.INFO)
    if args.debug:
        ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)
    # let's do this
    repos = make_repofiles_from_compose(args.compose_url, args.arch, args.repoid_prefix, args.enable_repos, args.disable_repos, dry_run=args.dry_run)
    if repos:
        return 0
    logger.error("No repository was found for given compose.")
    return 1

if __name__ == "__main__":
    sys.exit(main(*sys.argv[1:]))
