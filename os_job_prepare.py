#!/usr/bin/env python

import os
import requests
import sys

from beaker_xml import BeakerXml
from mapper_yaml import MapperYaml

'''
This prepares a 'fake' XML for this OSCI runner to crunch, updated based
on mapper.yaml in-repo information in order to run the tests in the very same way
as we do in beaker and Openstack.

Reasoning: Our internal CI system is based on producing and running a beaker job - XML
Our internal Openstack workflow too has been designed to run 'fake' job based on the same
XML generated for beaker - in order to keep both ways of execution the same,
regardless where the actual test job takes place.

And with the same reasoning we have decided that our Fedora OSCI run will take advantage
of the beaker (harness) as well as openstack runner code. Internally we work with a
real beaker XML - so for our openstack runner to work on OSCI enviroment - we simply
generate a very very simple 'beaker' xml here - following which the shared code
runs from there on (follows and executes beaker tasks - notably our harness code,
that takes care of the actual test run based on all stuff set in mapper.yaml in
each components repository).

'''

OUTPUT_PATH = 'beaker.xml'

os.environ['GIT_SSL_NO_VERIFY'] = 'true'

# location of the GITLAB group that contains tests. A repo under the group equals component (dist-git) name
gitlab_group_url = 'https://gitlab.com/dogtail'

# default branch to use to load the mapper and run the tests from
test_branch = 'master'
git_url = None

if len(sys.argv) > 2:
    test_branch = sys.argv[2]
if len(sys.argv) > 3:
    git_url = sys.argv[3]

template = "component_job_template.xml"

repo_name = sys.argv[1] # lets use params this easy for now

def get_mapper_yaml(repo_name, branch):
    link = '%s/%s/raw/%s/mapper.yaml' % (gitlab_group_url, repo_name, branch)
    print("Fetching mapper at: " + link)
    mapper = requests.get(link, verify=False).text
    return mapper


def export_beaker_xml(xml):
    print(">> Saving beaker xml to %s" % OUTPUT_PATH)
    full_output_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), OUTPUT_PATH)
    xml.write(full_output_path)
    # pretty the xml...
    os.system('xmllint --format %s --output %s' % (full_output_path, full_output_path))
    print("   wrote xml to %s" % full_output_path)
    return full_output_path


def update_xml_per_mapper(xml):
    mapper_text = get_mapper_yaml(repo_name, test_branch)
    print("Got Mapper:")
    print(mapper_text)
    mapper = MapperYaml(mapper_text, 'x86_64')
    testmapper = mapper.testmapper

    global git_url
    if git_url is None:
        git_url = gitlab_group_url + '/%s.git' % repo_name

    # The tests to run
    test_list = []
    test_list =  [x['testname'] for x in testmapper]

    if len(mapper.beaker_tasks) > 0:
        print('>>>> Found dependent tasks')
        for task_dict in mapper.beaker_tasks:
            task = ''
            if task_dict['name'].startswith('/desktop/rhel'):
                task = '/desktop/fedora/install'
            else:
                task = task_dict['name']
            params = task_dict['params']
            xml.append_task(task, params)

    if 'packages' in mapper.dependencies and mapper.dependencies['packages'] is not None:
        prepare_task_params = {}
        prepare_task_params['INSTALL_DEPS'] = ' '.join(mapper.dependencies['packages'])
        xml.append_task("/desktop/ci/prepare", prepare_task_params)

    print(">> Adding the harness task")
    harness_task_params = {}
    harness_task_params['GIT_REFSPEC'] = test_branch
    harness_task_params['COMPONENT'] = repo_name
    if len(test_list) > 0:
        harness_task_params['TESTS'] = ','.join(test_list)
    harness_task_params['GIT_URL'] = git_url
    xml.append_task('/desktop/ci/harness', harness_task_params)
    xml.append_task("/desktop/ci/prepare", {'COLLECT' : 'yes'})
    return xml


def run():
    xml = BeakerXml(template)
    xml = update_xml_per_mapper(xml)
    export_beaker_xml(xml)


if __name__ == "__main__":
    run()
