#!/usr/bin/env bash

set -x

# make swap

# fallocate -l 4G /mnt/4GB.swap
# chmod 600 /mnt/4GB.swap
# mkswap /mnt/4GB.swap
# swapon /mnt/4GB.swap
# sysctl vm.swappiness=10
# echo "/mnt/4GB.swap  none  swap  sw 0  0" >> /etc/fstab

#root password
echo "redhat" | passwd --stdin

#enable ssh password login
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl reload sshd.service

systemctl disable firewalld.service
systemctl stop firewalld.service

dnf -y install wget make git nano gcc pciutils screen at bzip2 libxml2 python3-iniparse python3-pyyaml python3-gitlab --skip-broken

# for when it fails from playbook misteryously
pushd /tmp/artifacts
screen -S lister -d -m -L php -S 0.0.0.0:80
popd

if [ ! -e /tmp/os_initial_setup_done ]; then

  useradd test

  # get rid of 1mt repo if exists to get rid of any potential conflicts with our repos
  # mv /etc/yum.repos.d/rhel.repo /etc/yum.repos.d/rhel.repo.1mt.back

  echo "redhat" | passwd test --stdin

  dnf -y install python3

  if [ ! -e /usr/bin/python ]; then
    ln -s /usr/bin/python3 /usr/bin/python
  fi

  # systemctl enable atd.service
  # systemctl start atd.service

  # pushd /etc/pki/ca-trust/source/anchors
  # wget --no-check-certificate https://password.corp.redhat.com/RH-IT-Root-CA.crt
  # update-ca-trust extract
  # popd

  # wget --no-check-certificate -O /etc/yum.repos.d/beaker-client.repo \
  #      http://download.lab.bos.redhat.com/beakerrepos/beaker-client-RedHatEnterpriseLinux.repo

  # repos started to get split, so we have to do this to make sure we have a valid (latest) repo subpath
  # export REPO_NUM=$(curl https://beaker.engineering.redhat.com/repos/ | grep -oP '<a href="(.*)/">'  | tail -n 1 | grep -oP '[0-9]*')

  # sed -i s/{REPONUM}/$REPO_NUM/ ./beaker-tasks.repo

  # cp beaker-tasks.repo /etc/yum.repos.d/

  # Yes the classic $releasever is inconsitent accors versions!
  # if [[ $1 == *"RHEL-8"* ]]; then
  #   sed -i s/{RHELVERSION}/8/ ./beaker-harness.repo
  # fi
  # if [[ $1 == *"RHEL-7"* ]]; then
  #   sed -i s/{RHELVERSION}/7/ ./beaker-harness.repo
  # fi
  # cp beaker-harness.repo /etc/yum.repos.d/
  #
  # yum install -y beakerlib beakerlib-redhat #rhts-test-env rhts-devel rhts-python beakerlib-redhat beaker-client beaker-redhat --skip-broken

  # yum install -y beaker-distribution-install.noarch
  # pushd /mnt/tests/distribution/install/
  # make run
  # popd

  # yum -y install desktop-ci-desktop-ci-harness desktop-ci-desktop-ci-prepare distribution-distribution-command \
  #  distribution-distribution-pkginstall distribution-distribution-Library-epel installation-installation-epel-install \
  #  tmp-desktop-ci-appstream-enable distribution-CoreOS-distribution-Library-Reboot rh-tests-distribution-utils-reboot.noarch \
  #  beaker-distribution-reservesys tmp-desktop-rhel7-install.noarch tmp-desktop-rhel8-install.noarch distribution-distribution-crashes-enable-abrt \
  #  distribution-distribution-install-brew-build distribution-distribution-setup_repository \
  #  distribution-distribution-addyumrepos tmp-desktop-fedora-install




  touch /tmp/os_initial_setup_done

fi


cp -r tasks/* /mnt/tests
# if [ ! -e /usr/bin/python ]; then
#   ln -s /usr/bin/python2 /usr/bin/python
# fi

# service atd start

# for when it fails from playbook misteryously
pushd /tmp/artifacts
screen -S lister -d -m -L php -S 0.0.0.0:80
popd

rm -f /usr/bin/rhts-run-simple-test
rm -f /usr/bin/rhts-report-result
rm -f /usr/bin/rhts-submit-log
rm -f /usr/bin/rstrnt-report-result
rm -f /usr/bin/rhts-abort

cp -f rhts-delegate /usr/bin/
chmod +x /usr/bin/rhts-delegate

ln -s /usr/bin/rhts-delegate /usr/bin/rhts-run-simple-test
ln -s /usr/bin/rhts-delegate /usr/bin/rhts-report-result
ln -s /usr/bin/rhts-delegate /usr/bin/rhts-submit-log
ln -s /usr/bin/rhts-delegate /usr/bin/rstrnt-report-result
ln -s /usr/bin/rhts-delegate /usr/bin/rhts-abort


# dnf config-manager --set-disabled rhel-8-buildroot
# dnf config-manager --set-disabled beaker-harness
# dnf config-manager --set-disabled beaker-harness-failsave

exit 0
