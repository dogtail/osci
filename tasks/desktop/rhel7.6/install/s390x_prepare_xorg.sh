#!/bin/bash
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Author: Vitezslav Humpa <vhumpa@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2014 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

set -x

VERSION=$(yum list xorg-x11-server-Xvfb | grep xorg-x11-server-Xvfb | awk '{ print $2 }')
PKG_NAME=xorg-x11-server
SERVER_FIND="global with_hw_servers 0"
SERVER_REPLACE="global with_hw_servers 1"

VERSION_DRV=\\.el7
PKG_NAME_DRV=xorg-x11-drv-dummy
DRV_FIND="\nExcludeArch: s390 s390x"
DRV_REPLACE=""

echo "Grabbing xorg-x11-server src.rpm..."
./brewgrab.py --id=${PKG_NAME} --arch=src --version=${VERSION}

PKG_SRC=$(ls ${PKG_NAME}*src.rpm)

echo "Grabbing xorg-x11-drv-dummy src.rpm..."
./brewgrab.py --id=${PKG_NAME_DRV} --arch=src --version=${VERSION_DRV}

PKG_SRC_DRV=$(ls ${PKG_NAME_DRV}*src.rpm)

echo "Installing Xorg build dependencies"
sudo yum -y install yum-utils rpm-build
sudo yum-builddep -y ${PKG_SRC}

######### See: https://bugzilla.redhat.com/show_bug.cgi?id=1447209#c1 ########
yum -y update http://download.eng.bos.redhat.com/brewroot/packages/binutils/2.27/7.base.el7/s390x/binutils-2.27-7.base.el7.s390x.rpm --nogpg
##############################################################################

echo Installing ${PKG_SRC}...
rpm -ivh ${PKG_SRC}

echo Installing ${PKG_SRC_DRV}...
rpm -ivh ${PKG_SRC_DRV}

echo  "Modifying specfiles to enable s390x builds ..."
sed -i "s/${SERVER_FIND}/${SERVER_REPLACE}/" ~/rpmbuild/SPECS/xorg-x11-server.spec
sed -i "N;s/${DRV_FIND}/${DRV_REPLACE}/" ~/rpmbuild/SPECS/xorg-x11-drv-dummy.spec

echo  "Building the server packages ..."
rpmbuild -bb ~/rpmbuild/SPECS/${PKG_NAME}.spec

echo  "Installing the xorg-x11-server-Xorg"
sudo yum -y install ~/rpmbuild/RPMS/$(arch)/xorg-x11-server-Xorg* ~/rpmbuild/RPMS/$(arch)/xorg-x11-server-common* ~/rpmbuild/RPMS/$(arch)/xorg-x11-server-devel*

echo "Installing driver build dependencies"
sudo yum-builddep -y ${PKG_SRC_DRV}

echo  "Building the driver package ..."
rpmbuild -bb ~/rpmbuild/SPECS/${PKG_NAME_DRV}.spec

echo  "Installing the xorg-x11-drv-dummy"
sudo yum -y install ~/rpmbuild/RPMS/$(arch)/xorg-x11-drv-dummy*

exit 0
