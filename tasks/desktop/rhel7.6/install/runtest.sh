#!/bin/bash
. /usr/bin/rhts-environment.sh

set -x

#set the root and test password to 'redhat' (for overcoming polkit easily)
echo "Setting root password to 'redhat'"
echo "redhat" | passwd root --stdin
echo "Setting test password to 'redhat'" # just in case
echo "redhat" | passwd test --stdin

#fake console
echo "Faking a console session..."
touch /var/run/console/test
echo test > /var/run/console/console.lock

#passwordless sudo
echo "enabling passwordless sudo"
if [ -e /etc/sudoers.bak ]; then
    mv -f /etc/sudoers.bak /etc/sudoers
fi
cp -a /etc/sudoers /etc/sudoers.bak
grep -v requiretty /etc/sudoers.bak > /etc/sudoers
echo 'Defaults:test !env_reset' >> /etc/sudoers
echo 'test ALL=(ALL)   NOPASSWD: ALL' >> /etc/sudoers

#setting ulimit to unlimited for test user
echo "ulimit -c unlimited" >> /home/test/.bashrc

#installation of desktop stuff
rhts-run-simple-test $TEST "grep test /etc/sudoers"
rhts-run-simple-test $TEST/user-to-video-grp "usermod -a -G video test"
rhts-run-simple-test $TEST/user-to-wheel-grp "usermod -a -G wheel test"

#needed when run on z-stream composes that may be older (DISTRO:RHEL-7.0)
rhts-run-simple-test $TEST/update-system "yum -y update"

mkdir -p /usr/lib64/python2.7/site-packages/dogtail_gui_helper
cp __init__.py gnome_apps_helper.py kde_apps_helper.py -t /usr/lib64/python2.7/site-packages/dogtail_gui_helper/

# delete when the repos are sane
export SKIP_BROKEN=yes
#grep Server /etc/redhat-release
#if [ $? -eq 0 ]; then
#    cp ./rhel-7.4-nightly-server.repo /etc/yum.repos.d/rhel-7.4-nightly-server.repo
#else
#    cp ./rhel-7.4-nightly.repo /etc/yum.repos.d/rhel-7.4-nightly.repo
#fi


if [ "$SKIP_BROKEN" == "yes" ]; then
    SKIP="--skip-broken"
fi

if [ "x$DESKTOP" == "xKDE" ]; then
    echo "export QT_ACCESSIBILITY=1" >> /home/test/.bashrc
    yum grouplist --verbose | grep graphical-server-environment
    if [ $? -eq 0 ]; then
        rhts-run-simple-test $TEST/kde-desktop-environment "yum -y install gdm kde-* -x *-debuginfo -x *-devel -x kde-l10n-* $SKIP" #KDE
    else
        rhts-run-simple-test $TEST/kde-desktop-environment "yum -y groupinstall kde-desktop-environment $SKIP" #KDE
    fi
    rhts-run-simple-test $TEST/qt-at-spi "yum -y --nogpg localinstall qt-at-spi*" # PPC vs. x86_64 will be autofiltered by yum
elif [ "x$DESKTOP" == "xALL" ]; then
    echo "export QT_ACCESSIBILITY=1" >> /home/test/.bashrc
    yum grouplist --verbose | grep graphical-server-environment
    if [ $? -eq 0 ]; then
        rhts-run-simple-test $TEST/kde-desktop-environment "yum -y install gdm kde-* -x *-debuginfo -x *-devel -x kde-l10n-* $SKIP" #KDE
        rhts-run-simple-test $TEST/gnome-desktop-environment "yum -y groupinstall graphical-server-environment $SKIP" #GNOME
    else
        rhts-run-simple-test $TEST/kde-desktop-environment "yum -y groupinstall kde-desktop-environment $SKIP" #KDE
        rhts-run-simple-test $TEST/gnome-desktop-environment "yum -y groupinstall gnome-desktop-environment $SKIP" #GNOME
    fi
    rhts-run-simple-test $TEST/qt-at-spi "yum -y --nogpg localinstall qt-at-spi*"
elif [ "x$DESKTOP" == "xMINIMAL" ]; then
    rhts-run-simple-test $TEST/gnome-desktop-minimal "yum -y install gdm gnome-shell gnome-session gnome-classic-session at-spi2-atk $SKIP" #GNOME
else
    yum grouplist --verbose | grep graphical-server-environment
    if [ $? -eq 0 ]; then
        export ARMFIX=''
        arch | grep aarch64
        if [ $? -eq 0 ]; then
            export ARMFIX='-x fwupdate* -x bpftool -x sos -x fwupd -x gnome-software -x abrt*'
        fi
        rhts-run-simple-test $TEST/gnome-desktop-environment "yum -y groupinstall graphical-server-environment $SKIP $ARMFIX"
    else
        rhts-run-simple-test $TEST/gnome-desktop-environment "yum -y groupinstall gnome-desktop-environment $SKIP"
    fi
fi

# delete when the repos are sane
#yum -y update --skip-broken

# Enable the dummy driver if this machine is headless.
# All video outputs available on the system are represented as directories
# under /sys/class/drm.  Each of them has the status file containing either
# "connected" or "disconnected".
if ! cat /sys/class/drm/*/status | egrep '^connected$'; then
    # We assume no s390x has a video card (that's why we don't ship Xorg for
    # that arch after all).
    arch | grep 's390'
    if [ $? -eq 0 ]; then
        echo "s390x: Building Xorg and the dummy driver"
        sh s390x_prepare_xorg.sh
        yum -y remove firstboot
    else
        /bin/cp 10-dummylibvnc.conf /etc/X11/xorg.conf.d
    fi
    echo "enabling dummy/void configuration for headless machine"
    /bin/cp xorg.conf /etc/X11
fi

if arch | grep -E 'ppc|x86_64|aarch64'; then
    rhts-run-simple-test $TEST/x-server "yum -y install xorg-x11-server-Xorg xorg-x11-drivers xorg-x11-server-Xvfb tigervnc-server tigervnc-server-module wpa_supplicant xterm"
fi

rhts-run-simple-test $TEST/x11vnc "yum -y --nogpg localinstall x11vnc-*.rpm libvncserver-*.rpm"
rhts-run-simple-test $TEST/dogtail-behave "yum -y --nogpg localinstall dogtail* python-enum* python-parse* python2-behave* python-six*"
# git is not preinstalled, if /distribution/install is ommited
rhts-run-simple-test $TEST/git "yum -y install git"
rhts-run-simple-test $TEST/pexpect "yum -y install pexpect"


if [ -e /usr/share/xsessions/1-kde-plasma-standard.desktop ]; then
    echo "making legacy symlink of kde-plasma session file"
    ln -s /usr/share/xsessions/1-kde-plasma-standard.desktop /usr/share/xsessions/kde-plasma.desktop
fi

# preinstall packages for abrt autoreporting task - as some dep issues may happen with errata prep
# dont make it report as subtest... we don't care too much if this fails
yum -y install abrt abrt-cli abrt-addon-ccpp libreport-plugin-logger libreport-plugin-ureport libreport-plugin-reportuploader libreport-plugin-mailx

# put in /etc/profile.d/, /etc/kde/env/
cp qt-at-spi.sh /etc/profile.d/
mkdir -p /etc/kde/env/
cp qt-at-spi.sh /etc/kde/env/

#install brewgrab
rhts-run-simple-test $TEST/brewgrab "/bin/cp brewgrab.py /usr/bin/brewgrab"
