#!/usr/bin/python

"""
 Module with helper class to create a basis for writing tests for KDE/QT
 apps or even GTK apps under KDE. All is present for basic acceptance,
 inherit from the KdeApp class to make a helper for specific KDE app.
"""

import sys
import time
import os
import re
import pwd
import traceback
import shlex
import types
import logging

from dogtail.utils import isA11yEnabled, enableA11y, screenshot
if isA11yEnabled() is False:
    print("kde-apps-helper: Enabling a11y")
    enableA11y(True)
    if isA11yEnabled() is False:
        time.sleep(5)
        print("Warning: second attempt to enable a11y")

from functools import partial
from time import time, sleep
from dogtail.tree import root, SearchError
from dogtail.rawinput import keyCombo, click, doubleClick, typeText, pressKey
from subprocess import Popen, PIPE
from gi.repository import Gdk
from gi.repository import GLib

stdout_prefix = '>>> >>> '
stderr_prefix = '!!! >>> '

# pretty print a standard message


def printOut(message):
    sys.stdout.write('%s %s\n' % (stdout_prefix, message))


# pretty print a message to stderr
def printError(message):
    sys.stderr.write('%s %s\n' % (stderr_prefix, message))


# returns integer representing pixel height of the screen
def getScreenHeight():
    return Gdk.Display.get_default().get_default_screen().get_root_window().get_height()


def printException():
    printOut("-- FAIL due to exception:")
    printOut('-' * 60)
    traceback.print_exc(file=sys.stdout)
    printOut('-' * 60)


def isProcessRunning(process):
    '''Gives true if process can be greped out of full ps dump '''
    s = Popen(["ps", "axw"], stdout=PIPE)
    for x in s.stdout:
        if re.search(process, x):
            return True
    return False

def waitForProcess(process, invert=False):
    '''Waits until a process appears'''
    while isProcessRunning(process) is invert:
        time.sleep(1)

def waitForSplash():
    '''Ends when no splash screen is found'''
    sleep(1)
    waitForProcess('kspash', invert=True)

def wait_until(tested, element=None, timeout=30, period=0.25):
    """
    This function keeps running lambda with specified params until the
    result is True or timeout is reached. Instead of lambda, Boolean variable
    can be used instead.
    Sample usages:
     * wait_until(lambda x: x.name != 'Loading...', context.app.instance)
       Pause until window title is not 'Loading...'.
       Return False if window title is still 'Loading...'
       Throw an exception if window doesn't exist after default timeout

     * wait_until(lambda element, expected: x.text == expected,
           element, ('Expected text'))
       Wait until element text becomes the expected (passed to the lambda)

     * wait_until(dialog.dead)
       Wait until the dialog is dead

    """
    if isinstance(tested, bool):
        curried_func = lambda: tested
    # or if callable(tested) and element is not None?
    elif isinstance(tested, types.FunctionType) and element is not None:
        curried_func = partial(tested, element)
    else:
        curried_func = tested

    exception_thrown = None
    mustend = int(time()) + timeout
    while int(time()) < mustend:
        try:
            if curried_func():
                return True
        except Exception as e:  # pylint: disable=broad-except
            # If lambda has thrown the exception we'll re-raise it later
            # and forget about if lambda passes
            exception_thrown = e
        sleep(period)
    if exception_thrown is not None:
        raise exception_thrown  # pylint: disable=raising-bad-type
    else:
        return False


class KdeApp(object):

    corner_distance = 10
    splashscreen_delay = 15  # time to wait for everything to load still under splash-screen

    def __init__(self, command, appname=None, app_description='', quit_shortcut='<Control><Q>', test=None, click_focus=True):
        """Inits the class instance with the information about a specific application

        @param command: a command to execute the app in terminal (without params! (use
        startViaCommand to do that))
        @param appname: a name of application as seen by a11y, only if different from command
        @param quit_shortcut:
        @param test: a name of the test to report to beaker, can be None
        @param click_focus: Set to False to disable focusing by clicking at window decorator in closeVia methods
        """
        if appname is None:
            appname = command.split()[0]
        if test is None:
            self.test = appname
        self.command = command
        self.appname = appname
        self.app_description = app_description
        self.test = test
        self.shortcut = quit_shortcut
        self.app = None
        self.click_focus = click_focus
        # Try to wait before a11y  bus gets initialized and starts responding
        # Waits for maximum about 3 minutes, before proceeding to likely fail
        for attempt in xrange(20):
            try:
                root.applications()
                break
            except GLib.GError:
                sleep(1)

    def getHighestPid(self):
        """ Gets the highest pid of all application processes """
        pipe = Popen('pgrep %s' % self.command, shell=True, stdout=PIPE).stdout
        # returns the highest pgreped pid
        try:
            return int(pipe.read().split()[-1])
        except IndexError:
            return None

    def clickFocus(self, maximize=None, dialog=False):
        """ Will focus on the app by clicking in the middle of its window titlebar """
        try:
            main_win = self.app.child(roleName='dialog' if dialog is True else 'window', recursive=False)
            coordinates = (main_win.position[0] + main_win.size[0] / 2, main_win.position[1] - 10)
            if maximize is None:
                click(coordinates[0], coordinates[1])
            else:  # a doubleClick to maximize as well
                doubleClick(coordinates[0], coordinates[1])
        except:
            printException()
            return False

    def startViaMenu(self):
        """ Will run the app through the standard application launcher """
        try:
            waitForSplash()
            height = Gdk.Display.get_default().get_default_screen().get_root_window().get_height()
            click(self.corner_distance, height - self.corner_distance)
            plasma = root.application('plasma-desktop')
            plasma.child(name='Search:', roleName='label').click()
            typeText(self.command)
            sleep(1)
            pressKey('enter')
            assert wait_until(lambda x: x.isAccessible(), self, timeout=30),\
                "Application failed to start"
        except:
            printException()
            return False
        self.__PID = self.getHighestPid()
        return self.checkRunning('Running %s via menu search' % self.appname)

    def startViaKRunner(self):
        """ Simulates running app through Run command interface (alt-F2...)"""
        try:
            waitForSplash()
            os.system('krunner')
            sleep(1.5)
            typeText('%s' % self.command)
            sleep(2)
            pressKey('enter')
            assert wait_until(lambda x: x.isAccessible(), self, timeout=30),\
                "Application failed to start"
        except:
            printException()
            return False
        self.__PID = self.getHighestPid()
        return self.checkRunning('Running %s via menu Run Command Interface' % self.appname)

    def startViaCommand(self, params='', timeout=30):
        """ Directly executes the application, independent from the Desktop layout """
        try:
            waitForSplash()
            if len(params) > 0:
                params = " " + params
            self.process = Popen(shlex.split(self.command + params),
                             stdout=PIPE, stderr=PIPE, bufsize=0)
            self.__PID = self.process.pid
            assert wait_until(lambda x: x.isAccessible(), self, timeout=timeout),\
                "Application failed to start"
        except:
            printException()
            return False
        return self.checkRunning('Running %s via command' % self.appname)

    def checkRunning(self, message, terminate=False):
        """ Checks whether the application is running, will also
            see if it crashed and made a core dump; Will write
            a rhts result, thus serves as a test checkpoint"""
        result = terminate
        if self.isAccessible():
            printOut('%s is running!' % self.appname)
            result = not terminate
            if terminate:
                self.kill()
        screenshot()
        self.writeResult(message, result)
        return result

    def closeViaMenu(self, menu='File', menuitem='Quit', dialog=False):
        """ Does execute 'Quit' item in the main menu """
        try:
            if not wait_until(lambda x: x.isAccessible(), self, timeout=5):
                return False
            if self.click_focus:
                self.clickFocus(dialog=dialog)
            self.app.child(name=menu, roleName='menu item').click()
            sleep(1)
            self.app.child(name=menuitem, roleName='menu item').click()
        except:
            printException()
            return False
        wait_until(lambda x: not x.isAccessible(), self, timeout=30)
        return self.checkRunning('Quiting %s through menu' % self.appname, True)

    def closeViaShortcut(self, dialog=False):
        """ Exit the application through the predefined keyboard shortcut """
        try:
            if not wait_until(lambda x: x.isAccessible(), self, timeout=5):
                return False
            if self.click_focus:
                self.clickFocus(dialog=dialog)
            keyCombo(self.shortcut)
        except:
            printException()
            return False
        wait_until(lambda x: not x.isAccessible(), self, timeout=30)
        return self.checkRunning('Quiting %s with shortcut' % self.appname, True)

    def writeResult(self, description, result):
        """
        Write a test result on stdout and using rhts-report-result.

        @param description: Short description of executed test.
        @type description: String
        @param result: Result of the test.
        @type result: Boolean
        """
        if result:
            result = "PASS"
            printOut("%s: %s" % (description, result))
        else:
            result = "FAIL"
            printError("%s: %s" % (description, result))
        if pwd.getpwuid(os.getuid())[0] == 'test':
            subtestname = description.replace(' ', '-')
            log = '/dev/null'
            cmd = 'sudo rhts-report-result %s/%s %s %s' % (self.test, subtestname,
                                                           result, log)
            Popen(cmd, shell=True).wait()

    def getPid(self):
        return os.system('pidof %s |wc -w' % self.command)

    def isAccessible(self):
        """ Returns true if the application is visible under the AT-SPI
            root desktop """
        try:
            self.app = root.child(name=self.appname, roleName='application',
                                  description=self.app_description, retry=False, recursive=False)
            printOut("%s is accessible" % self.appname)
            return True
        except SearchError:
            printError("%s couldn't be found" % self.appname)
            return False

    def signal(self, signal):
        """ Sends a singal to the latest app process """
        if self.getHighestPid() == None:
            printError('%s cant be signaled!' % self.command)
            return
        return Popen("kill -%d %d" % (signal, self.getHighestPid()), shell=True).wait()

    def terminate(self):
        """ Invoke sigterm on latest application process"""
        self.signal(15)
        sleep(1)
        if hasattr(self, 'process'):
            self.process.wait()
        result = True
        if self.getHighestPid() == self.__PID:
            printError('%s did not terminate!' % self.command)
            result = False
        self.writeResult('Terminating %s' % self.command, result)

    def kill(self):
        """ 'There can be no discussion that you will end!' """
        self.signal(9)
