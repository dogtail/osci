#!/bin/bash

set -x

export SOURCE_NAME=rel-eng
export DATE_STRING=latest-AppStream-8

echo $DISTRO | grep RHEL-8
if [ $? -eq 0 ]; then
  export DATE_STRING=AppStream-8.0-`echo $DISTRO | cut -d "-" -f3`
  echo $DISTRO | grep .n.
  if [ $? -eq 0 ]; then
    export SOURCE_NAME=nightly
  fi
fi

if [ -n "$APPSTREAM" ]; then # param overrides any autodetection
  export SOURCE_NAME=rel-eng
  echo $APPSTREAM | grep .n.
  if [ $? -eq 0 ]; then
    export SOURCE_NAME=nightly
  fi
  export DATE_STRING=$APPSTREAM
fi

export rc=0
# test resulting repo path exists
export baseurl=http://download-node-02.eng.bos.redhat.com/$SOURCE_NAME/$DATE_STRING/compose/AppStream/x86_64/os/
curl -i $baseurl | grep "404 Not Found"
if [ $? -eq 0 ]; then
  export rc=1
  echo "The resulting repo wasn't found!"
  export DATE_STRING=latest-AppStream-8
fi

sed -i s/{source}/$SOURCE_NAME/ ./AppStream.repo
sed -i s/{datestring}/$DATE_STRING/ ./AppStream.repo

cp ./AppStream.repo /etc/yum.repos.d/

exit $rc
