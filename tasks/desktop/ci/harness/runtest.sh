#!/bin/bash

. /usr/bin/rhts-environment.sh

echo $'\n----------------------------'
echo "Dumping ENV:"
echo "----------------------------"
env

# echo $'\n----------------------------'
# echo "Test Setup:"
# echo "----------------------------"
TEMPORARYDIR=$(mktemp -d)
#pushd $TEMPORARYDIR
#echo 'Temporary Work Directory:' $TEMPORARYDIR
export TEMPORARYDIR=$TEMPORARYDIR
export PYTHONUNBUFFERED=1

echo $'\n----------------------------'
echo "List of test parameters"
echo "----------------------------"
echo "ARCH: " $ARCH
echo "DISTRO: " $DISTRO
echo "FAMILY: " $FAMILY
echo "HOSTNAME: " $HOSTNAME
echo "HOSTTYPE: " $HOSTTYPE
echo "LAB_CONTROLLER: " $LAB_CONTROLLER
echo "LOGNAME: " $LOGNAME
echo "RECIPEID: " $RECIPEID
echo "RECIPESETID: " $RECIPESETID
echo "RECIPETESTID: " $RECIPETESTID
echo "RECIPETYPE: " $RECIPETYPE
echo "TASKID: " $TASKID
echo "TESTID: " $TESTID
echo "TESTPATH: " $TESTPATH
echo "TESTRPMNAME: " $TESTRPMNAME
echo "TESTVERSION: " $TESTVERSION
echo "SUBMITTER: " $SUBMITTER
echo "PYTHONPATH: " $PYTHONPATH
echo "DEBUG: " $DEBUG
echo "TYPE: " $TYPE
echo "TESTS: " $TESTS
echo "GIT_URL: " $GIT_URL
echo "GIT_TARGETBRANCH: " $GIT_TARGETBRANCH
echo "GIT_TAGORCOMMITID: " $GIT_TAGORCOMMITID
echo "GIT_REFSPEC: " $GIT_REFSPEC
echo "WGET: " $WGET_URL
echo "PACKAGEINSTALLS: " $PACKAGEINSTALLS
echo "PACKAGECHECKER: " $PACKAGECHECKER


echo $'\n----------------------------'
echo "Installing pip and friends"
echo "----------------------------"

yum -y install git PyYAML python-setuptools
yum -y install python2-setuptools # el8
yum -y install python2-pyyaml # el8

cat /etc/redhat-release | grep 'Fedora'  # Do not do on fedora!
if [ $? -ne 0 ]; then
  if [ ! -e /usr/bin/pip-2.7 ]; then # fallback pip install
    easy_install-2.7 pip
  fi
fi

# create python-gitlab.cgf
cp python-gitlab.cfg /etc/python-gitlab.cfg
cp python-gitlab.cfg ~/.python-gitlab.cfg

cat /etc/redhat-release | grep 'release 6'
if [ $? -eq 0 ]; then
  echo "This is RHEL-6.x, compiling PYTHON-2.7"
  yum -y install autoconf bluez-libs-devel bzip2 bzip2-devel expat-devel findutils gcc-c++ gdbm-devel glibc-devel gmp-devel libGL-devel libX11-devel libdb-devel libffi-devel ncurses-devel openssl-devel pkgconfig readline-devel sqlite-devel systemtap-sdt-devel tar tcl-devel tix-devel tk-devel valgrind-devel zlib-devel
  wget https://www.python.org/ftp/python/2.7.5/Python-2.7.5.tgz
  tar xzf Python-2.7.5.tgz
  pushd Python-2.7.5
    ./configure
    make altinstall
    python2.7 --version
  popd
  # setuptools for pip
  wget https://pypi.python.org/packages/0f/22/7fdcc777ba60e2a8b1ea17f679c2652ffe80bd5a2f35d61c629cb9545d5e/setuptools-36.7.2.zip
  unzip setuptools-36.7.2.zip
  pushd setuptools-36.7.2
    python2.7 setup.py install
  popd
  # pip for installing python-gitlab and PyYAML
  wget https://pypi.python.org/packages/11/b6/abcb525026a4be042b486df43905d6893fb04f05aac21c32c638e939e447/pip-9.0.1.tar.gz
  tar xzf pip-9.0.1.tar.gz
  pushd pip-9.0.1
    python2.7 setup.py install
  popd


  echo $'\n----------------------------'
  echo "Test Setup and Execute via harness.py:"
  echo "----------------------------"

  pip2.7 install python-gitlab
  pip2.7 install PyYAML
  python2.7 -u ./harness.py
  export RC=$?
else
  echo $'\n----------------------------'
  echo "Test Setup and Execute via harness.py:"
  echo "----------------------------"

  cat /etc/redhat-release | grep 'Fedora'
  if [ $? -eq 0 ]; then
    dnf -y install python3-requests python3-pyyaml python3-pip --skip-broken
    python3 -m pip install python-gitlab
    echo "\n\nrunning: python3 ./harness.py"
    python3 ./harness.py
    export RC=$?
  else
    ln -s /usr/bin/python2 /usr/bin/python
    cat /etc/redhat-release | grep 'release 8'
    if [ $? -eq 0 ]; then
      dnf -y install python3-requests python3-pyyaml python3-pip --skip-broken
      if [ $? -ne 0 ]; then
        sleep 5
        dnf -4 -y install python3-requests python3-pyyaml python3-pip --skip-broken
        if [ $? -ne 0 ]; then
          sleep 5
          dnf -4 -y install python3-requests python3-pyyaml python3-pip --skip-broken
        fi
      fi
      /usr/libexec/platform-python -m pip install python-gitlab
      /usr/libexec/platform-python -u ./harness.py
      export RC=$?
    else
      python2.7 -m pip install requests
      python2.7 -m pip install python-gitlab
      python2.7 -u ./harness.py
      export RC=$?
    fi
  fi
fi

echo $'\n----------------------------'
echo "Test Conclusion:"
echo "----------------------------"

mkdir -p /tmp/artifacts

if [ $RC -eq 0 ];then
    echo "Success: All tests passed"
    echo 0 > /tmp/artifacts/harness_rc.txt
else
    echo "Warning: Abnormal HARNESS termination or one or more tests failed"
    echo 1 > /tmp/artifacts/harness_rc.txt
fi

exit 0
