import yaml
import copy
import traceback
import gitlab
import sys
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

from gitlab.exceptions import GitlabGetError

testmapper_template = {'testname' : None,
    'arches-exclude' : None,
    'dir' : '.',
    'run' : None,
    'package-deps' : None,
    'timeout' : '10m',
    'feature' : None,
    'tags' : []}

gl_api = gitlab.Gitlab.from_config()
gl_project = None

dqe_group_id = 1829
dqe_group_name = 'desktopqe'
skip_projects = ['tools', 'install', 'desktop-ci']

all_component_mappers = {}

def get_gitlab_project(repo=None):
    '''Returns the gitlab project handler, spits GitlabGetError if it cant get the repo'''
    global gl_project
    if repo is not None:
        if gl_project is None or gl_project.name != repo:
            print(">> Getting gitlab project " + 'desktopqe/%s' % repo.strip())
            gl_project = gl_api.projects.get('desktopqe/%s' % repo.strip())
    return gl_project


def check_repo_exists(reponame, die=True):
    try:
        get_gitlab_project(reponame)
        return True
    except Exception:
        print('Caught exception trying to obtain Gitlab repo object. Does the repo exist?')
        traceback.print_exc(file=sys.stdout)
        if die:
            sys.exit(1)
        return False


def get_mapper(reponame, refspec, die=True):
    try:
        project = get_gitlab_project(reponame)
    except Exception:
        print('Caught exception trying to obtain Gitlab repo object. Does the repo exist?')
        traceback.print_exc(file=sys.stdout)
        if die:
            sys.exit(1)
        else:
            return None
    try:
        f = project.files.get(file_path='mapper.yaml', ref=refspec)
    except Exception:
        print('Caught exception trying to get mapper.yaml from the gitlab repo. Does it exist in branch: %s?' % refspec)
        traceback.print_exc(file=sys.stdout)
        if die:
            sys.exit(1)
        else:
            return None
    return f.decode()


def get_all_gitlab_projects(lazy=False):
    projects = []
    print('Getting all group projects')
    group = gl_api.groups.get(dqe_group_id)
    group_projects = group.projects.list(all=True) # group_project isn't a full object, need to reinitialize
    print('Converting to actual projects (lazy:%s)' % str(lazy))
    for gp in group_projects:
        p = gl_api.projects.get(gp.id, lazy=lazy) # lazy creates shallow object, do we want this?
        if lazy:
            p.name = gp.name
        if p.name not in skip_projects:
            projects.append(p)
    return projects


# get list of comps and subcomps based on all projects in gitlab and given refspec(branch)
def get_components_list(refspec, store_mappers=False, store_file_objects=False):
    print('Getting all project objects from the gitlab group...')
    projects = get_all_gitlab_projects(lazy=True)
    print('Found %d projects' % len(projects))
    component_list = []
    for project in projects:
        print('Scanning for mapper yaml in: %s, refspec: %s' % (project.name, refspec))
        try:
            f = project.files.get(file_path='mapper.yaml', ref=refspec)
            if store_mappers:
                content = f.decode()
                all_component_mappers[project.name] = content
                if store_file_objects:
                    all_component_mappers[project.name] = (content, f)
        except GitlabGetError:
            continue
        component_list.append(project.name)
    return component_list


def get_all_component_mappers(refspec, store_file_objects=False):
    global all_component_mappers
    if len(all_component_mappers) == 0:
        get_components_list(refspec, store_mappers=True, store_file_objects=store_file_objects)
    return all_component_mappers


class MapperYaml(object):

    # mapper can be string of file descriptor
    def __init__(self, mapper, arches_list, relevant_plan_type='tcms'):
        self._all_arches = arches_list
        self.relevant_plan_type = relevant_plan_type
        self.m = yaml.load(mapper)
        self._sub_objects = None
        self.component = self.m['component']
        if 'dependencies' in self.m:
            self._deps = self.m['dependencies']
        if 'testmapper' in self.m:
            self._testmapper = self.m['testmapper']
        if 'setup' in self.m:
            self.setup = self.m['setup']
        if 'cleanup' in self.m:
            self.cleanup = self.m['cleanup']
        if 'beaker-hardware' in self.m:
            self.beaker_hardware = self.m['beaker-hardware']

    def __get_supported_arches(self):
        if 'arches' in self.component:
            if len(self.component['arches']) == 1 and self.component['arches'][0].lower() == 'all':
                return self._all_arches
            return self.component['arches']
        elif 'arches-exclude' in self.component:
            return [x for x in self._all_arches if x not in self.component['arches-exclude']]
        else:
            return self._all_arches

    @property
    def supported_arches(self):
        return self.__get_supported_arches()

    def is_arch_supported(self, arch):
        return arch in self.__get_supported_arches()

    @property
    def default_test_timeout(self):
        if 'test-timeout' in self.component:
            return self.component['test-timeout']
        else:
            return '10m'

    @property
    def gate_environment(self):
        if 'gate-in' in self.component:
            return self.component['gate-in']
        else:
            return 'openstack'

    @property
    def default_run_command(self):
        if 'test-run' in self.component:
            return self.component['test-run'].replace('$testname', '%s')
        else:
            return './runtest.sh %s'

    @property
    def plan_id(self):
        if self.relevant_plan_type in self.component:
            return str(self.component[self.relevant_plan_type])
        return None

    @property
    def tags(self):
        if 'tags' in self.component:
            return self.component['tags'].split()
        return []

    def get_hardware_req_for_arch(self, arch):
        full_hw_template = {'group': None,
                            'hostname': None,
                            'kernel-options': None,
                            'kernel-options-post': None,
                            'variant': None}
        result = copy.deepcopy(full_hw_template)
        if arch in [list(x.keys())[0] for x in self.beaker_hardware]:
            arch_dict = [x for x in self.beaker_hardware if arch in list(x.keys())][0][arch]
            for key in full_hw_template.keys():
                if key in arch_dict:
                    result[key] = arch_dict[key]
            return result
        return full_hw_template

    @property
    def beaker_tasks(self):
        result = []
        for entry in self.dependencies['beaker-tasks']:
            if type(entry) is dict:
                result.append({'name' : list(entry.keys())[0], 'params' : entry[list(entry.keys())[0]]})
            else:
                result.append({'name' : entry, 'params' : None})
        return result


    @property
    def subcomponents(self):
        if self._sub_objects is None:
            self._sub_objects = []
            if 'subcomponents' in self.component:
                for compdict in self.component['subcomponents']:
                    self._sub_objects.append(Subcomponent(self, compdict)) # , self._all_arches, self.relevant_plan_type, self._testmapper))
        return self._sub_objects

    @property # reconstruct the dict to add None where actually missing in the yaml
    def dependencies(self):
        template = {'packages' : None, 'pip' : None, 'pip2' : None, 'pip3' : None, 'beaker-tasks' : None}
        if 'packages' in self._deps:
            template['packages'] = self._deps['packages']
        if 'pip' in self._deps:
            template['pip'] = self._deps['pip']
        if 'pip2' in self._deps:
            template['pip2'] = self._deps['pip2']
        if 'pip3' in self._deps:
            template['pip3'] = self._deps['pip3']
        if 'beaker-tasks' in self._deps:
            template['beaker-tasks'] = self._deps['beaker-tasks']
        return template


    def _parse_testmapper_section(self, section):
        full_entries = []
        for entry in section:
            instance = copy.deepcopy(testmapper_template)
            instance['testname'] = entry
            instance['timeout'] = self.default_test_timeout
            if type(entry) is dict:
                try:
                    instance['testname'] = list(entry.keys())[0]
                    if 'arches-exclude' in entry[instance['testname']]:
                        instance['arches-exclude'] = entry[instance['testname']]['arches-exclude']
                    if 'dir' in entry[instance['testname']]:
                        instance['dir'] = entry[instance['testname']]['dir']
                    if 'feature' in entry[instance['testname']]:
                        instance['feature'] = entry[instance['testname']]['feature']
                    if 'tags' in entry[instance['testname']]:
                        instance['tags'] = entry[instance['testname']]['tags'].split()
                    if 'run' in entry[instance['testname']]:
                        instance['run'] = entry[instance['testname']]['run']
                        if '$testname' in instance['run']:
                            instance['run'].replace('$testname', instance['testname'])
                    else:
                        instance['run'] = self.default_run_command % instance['testname']
                    if 'package-deps' in entry[instance['testname']]:
                        instance['package-deps'] = entry[instance['testname']]['package-deps']
                    if 'timeout' in entry[instance['testname']]:
                        instance['timeout'] = entry[instance['testname']]['timeout']
                except Exception as e:
                    traceback.print_exc(file=sys.stdout)
                    print('Error parsing the entry. Perhaps incorrect scope? (extra ":" etc.):')
                    print(entry)
                    raise e
            else:
                instance['run'] = self.default_run_command % instance['testname']
            full_entries.append(instance)
        return full_entries

    @property
    def testmapper(self):
        default = self._testmapper
        if type(self._testmapper) is dict and 'default' in self._testmapper:
            default = self._testmapper['default']
        return self._parse_testmapper_section(default)

    def get_tagged_testcases(self, tag):
        return [x['testname'] for x in self.testmapper if tag in x['tags']]


class Subcomponent(object):
    def __init__(self, parent, compdict):
        self._dict = compdict
        self.parent = parent
        self._testmapper = parent._testmapper
        self.name = list(compdict.keys())[0]
        self.component = compdict[self.name]
        self.supported_arches = parent._all_arches
        if 'arches' in self.component:
            self.supported_arches = self.component['arches']
        elif 'arches-exclude' in self.component:
            self.supported_arches = [x for x in parent._all_arches if x not in self.component['arches-exclude']]

    @property
    def plan_id(self):
        if self.parent.relevant_plan_type in self.component:
            return str(self.component[self.parent.relevant_plan_type])
        return None

    @property
    def gate_environment(self):
        if 'gate-in' in self.component:
            return self.component['gate-in']
        else:
            return 'openstack'

    @property
    def tags(self):
        if 'tags' in self.parent.component:
            return self.parent.component['tags'].split()
        return None

    @property
    def testmapper(self):
        if type(self._testmapper) is dict and self.name in self._testmapper:
            return self.parent._parse_testmapper_section(self._testmapper[self.name])

    def get_tagged_testcases(self, tag):
        return [x['testname'] for x in self.testmapper if tag in x['tags']]

    def get_hardware_req_for_arch(self, arch):
        if 'subcomponents' not in [list(x.keys())[0] for x in self.parent.beaker_hardware]:
            return self.parent.get_hardware_req_for_arch(arch)
        if self.name not in [x for x in self.parent.beaker_hardware if 'subcomponents' in list(x.keys())][0]['subcomponents']:
            return self.parent.get_hardware_req_for_arch(arch)
        sub_hw_dict = [x for x in self.parent.beaker_hardware if 'subcomponents' in list(x.keys())][0]['subcomponents'][self.name]
        full_hw_template = {'group': None,
                            'hostname': None,
                            'kernel-options': None,
                            'kernel-options-post': None,
                            'variant': None}
        result = copy.deepcopy(full_hw_template)
        if arch in [list(x.keys())[0] for x in sub_hw_dict]:
            arch_dict = [x for x in sub_hw_dict if arch in list(x.keys())][0][arch]
            for key in full_hw_template.keys():
                if key in arch_dict:
                    result[key] = arch_dict[key]
            return result
        return full_hw_template
