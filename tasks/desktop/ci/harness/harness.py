#!/usr/bin/env python

import os
import sys
import logging
import subprocess
import signal
import traceback
import re
from pprint import pprint
from mapper_yaml import MapperYaml
from threading import Timer

logging.basicConfig(format='%(asctime)s >>> %(message)s',  datefmt='%H:%M:%S')
log = logging.getLogger('harness')

# if 'DEBUG' in os.environ:
#     log.setLevel(logging.DEBUG)
# else:
#     log.setLevel(logging.INFO)
log.setLevel(logging.DEBUG)

clone_dir = '/mnt/tests'
default_git_url = 'https://gitlab.cee.redhat.com/desktopqe/{comp}.git'

# these are some defaults we need to pass to MapperYaml, however these are not really
# relevant for the MapperYaml using within harness (contra builder code)
default_plan_id_type = 'tcms'
arches_list = ['x86_64', 'ppc64', 'ppc64le', 's390x', 'aarch64']

settings = {}

os.environ['GIT_SSL_NO_VERIFY'] = 'true'

def read_env_options():
    log.info('>> Reading env options')

    settings['git_url'] = None
    if 'GIT_URL' in os.environ and os.environ['GIT_URL']:
        settings['git_url'] = os.environ['GIT_URL']
    else:
        die("Cannot continue without GIT_URL defined!")

    settings['git_refspec'] = None
    if 'GIT_TARGETBRANCH' in os.environ:
        settings['git_refspec'] = os.environ['GIT_TARGETBRANCH']
    if 'GIT_TAGORCOMMITID' in os.environ:
        settings['git_refspec'] = os.environ['GIT_TAGORCOMMITID']
    if 'GIT_REFSPEC' in os.environ:
        settings['git_refspec'] = os.environ['GIT_REFSPEC']

    if settings['git_refspec'] is None:
        die('Cannot continue without some GIT_REFSPEC defined!')

    settings['is_subcomponent'] = False
    settings['component'] = get_git_directory_name()
    if 'COMPONENT' in os.environ:
        settings['component'] = os.environ['COMPONENT'].strip()
        if settings['component'] != get_git_directory_name():
            settings['is_subcomponent'] = True

    settings['tests'] = ''
    if 'TESTS' in os.environ:
        settings['tests'] = os.environ['TESTS']

    settings['debug'] = 'true'  # for sure at least for now
    if 'DEBUG' in os.environ:
        settings['debug'] = os.environ['DEBUG']

    settings['repo_dir'] = os.path.join(clone_dir, get_git_directory_name())


    log.info('settings:')
    pprint(settings)


def get_command_output_or_die(cmd, error_message):
    output = ''
    try:
        output = subprocess.check_output(cmd.split(), env=os.environ, stderr=subprocess.STDOUT).decode('utf-8')
    except subprocess.CalledProcessError as e:
        log.info(error_message)
        log.info('Return Code: %d' % e.returncode)
        log.info('Output:')
        log.info(e.output)
        die("Exiting.")
    return output


def clone_refspec():
    git_version = subprocess.check_output('rpm -q git', shell=True).decode('utf-8')
    shallow = ''
    if 'git-1.7' not in git_version and 'git-1.6' not in git_version:
        shallow = '--depth 5 --no-single-branch'
    cmd = 'git clone %s %s' % (shallow, settings['git_url'])
    output = ''
    pull = False
    if not os.path.isdir("./%s" % get_git_directory_name()):
        log.info('Cloning GIT repo, executing: %s ' % cmd)
        output = get_command_output_or_die(cmd, 'Error: The GIT returned a non-zero value')
        log.info('Output:')
        log.info(output)
    else:
        pull = True
        log.info('A git repo already appears to be cloned.')
    log.info('chddir to git dir, should be: %s ' % get_git_directory_name())
    os.chdir(get_git_directory_name())
    log.info('Checking out destination refspec: %s' % settings['git_refspec'])
    cmd = 'git checkout %s' % settings['git_refspec']
    output = get_command_output_or_die(cmd, 'Error: The GIT checkout returned a non-zero value')
    log.info('Output: ' + output)
    if pull is True:
        log.info('Pulling the repository for changes:')
        cmd = 'git pull'
        output = get_command_output_or_die(cmd, 'Error: The GIT pull returned a non-zero value')
        log.info('Output: ' + output)
    log.info('Initializing submodules:')
    cmd = 'git submodule update --init --recursive'
    output = subprocess.check_output(cmd.split(), env=os.environ, stderr=subprocess.STDOUT).decode('utf-8')
    log.info(output)



def install_dependencies(mapper):
    log.info('============ Dependency install ============')
    if 'packages' in mapper.dependencies and mapper.dependencies['packages']:
        packages = ' '.join(mapper.dependencies['packages'])
        log.info('Packages defined: %s' % packages)
        cmd = 'yum -y install %s' % packages
        log.info('Installing packages, executing: %s ' % cmd)
        output = get_command_output_or_die(cmd, 'Error: YUM/DNF returned a non-zero value')
        log.info('Output:')
        log.info(output)
    if 'pip' in mapper.dependencies and mapper.dependencies['pip']:
        modules = ' '.join(mapper.dependencies['pip'])
        log.info('Pip modules defined: %s' % modules)
        cmd = 'pip2 install %s' % modules
        log.info('Installing modules, executing: %s ' % cmd)
        output = get_command_output_or_die(cmd, 'Error: PIP returned a non-zero value')
        log.info('Output:')
        log.info(output)
    if 'pip2' in mapper.dependencies and mapper.dependencies['pip2']:
        modules = ' '.join(mapper.dependencies['pip2'])
        log.info('Pip modules defined: %s' % modules)
        cmd = 'pip2 install %s' % modules
        log.info('Installing modules, executing: %s ' % cmd)
        output = get_command_output_or_die(cmd, 'Error: PIP returned a non-zero value')
        log.info('Output:')
        log.info(output)
    if 'pip3' in mapper.dependencies and mapper.dependencies['pip3']:
        modules = ' '.join(mapper.dependencies['pip3'])
        log.info('Pip modules defined: %s' % modules)
        cmd = 'pip3 install %s' % modules
        log.info('Installing modules, executing: %s ' % cmd)
        output = get_command_output_or_die(cmd, 'Error: PIP returned a non-zero value')
        log.info('Output:')
        log.info(output)


def do_setup(mapper, cleanup=False):
    action = 'setup'
    if cleanup:
        action = 'cleanup'
    if action in mapper.m and mapper.m[action] is not None and mapper.m[action] != '':
        script = mapper.m[action].strip()
        with open('./%s_script.sh' % action, 'w') as f:
            f.write(script)
        log.info('Executing the %s action' % action)
        output = get_command_output_or_die('sh ./%s_script.sh' % action, 'Error: %s returned a non-zero value' % action)
        log.info('Output: %s' % output)


def get_git_directory_name():
    #import ipdb; ipdb.set_trace()
    basename = os.path.basename(settings['git_url'])
    if basename.endswith('.git'):
        basename = re.sub(r"\.git$", "", basename)
    return basename


def get_mapper_yaml():
    content = ''
    if os.path.isfile('./mapper.yaml'):
        with open('./mapper.yaml', 'r') as f:
            content = f.read()
    else:
        die("Didn't find a mapper.yaml file in the repository/refspec!")
    return content


def filter_tests_to_run(mapper):
    arch = subprocess.check_output('arch', stderr=subprocess.STDOUT, shell=True).decode('utf-8').strip()
    testmapper = mapper.testmapper
    if settings['is_subcomponent']:
        subcomponent = [x for x in mapper.subcomponents if x.name == settings['component']][0]
        testmapper = subcomponent.testmapper
    filtered_tests = []
    if settings['tests']:
        testnames =  [x.strip() for x in settings['tests'].strip(',').split(',')]
        filtered_tests = [x for x in testmapper if x['testname'] in testnames]
    else:
        filtered_tests = testmapper
    # finally filter out tests disabled on THIS arch
    return [x for x in filtered_tests if not (x['arches-exclude'] is not None and arch in x['arches-exclude'])]


def get_time_seconds(time_str):
    if type(time_str) is int:
        time_str = str(time_str)
    secs = 0
    minutes = 0
    hours = 0
    days = 0
    try:
        for t in time_str.split():
            if t.endswith('d'):
                days = int(t.strip('d'))
            elif t.endswith('h'):
                hours = int(t.strip('h'))
            elif t.endswith('m'):
                minutes = int(t.strip('m'))
            elif t.endswith('s'):
                secs = int(t.strip('s'))
            else:
                secs = int(t.strip())
        s = 24 * 3600 * days + 3600 * hours + 60 * minutes + secs
    except Exception as e:
        print("Invalid time string '%s' given! (example: 1d 5h 5s)" % time_str)
        raise e
    return s


class TestRunner(object):

    PASS = 0
    FAIL = 1
    TIMEOUT = 2

    def __init__(self, tests):
        self.tests = tests
        self.__proc = None
        self.timeouted = False

    @property
    def process(self):
        return self.__proc

    @process.setter
    def process(self, value):
        self.__proc = value


    def run_tests(self):
        resultlist = []
        index = 0
        log.info('Tests about to run:')
        for test_dict in self.tests:
            log.info("%d: %s" % (index, test_dict['testname']))
            index += 1
        index = 0
        for test_dict in self.tests:
            print('')
            log.info('============ Running test %03d: %s ============' % (index, test_dict['testname']))

            # Set $TEST environment variable here for beaker logs to contain the component, test index and test name
            envtestnamevar = '%s_Test%03d_%s' % (settings['component'], index, test_dict['testname'])

            log.info('Setting up TEST environment variable: %s' % envtestnamevar)
            os.putenv('TEST', envtestnamevar)
            os.environ['TEST'] = envtestnamevar
            self.envtestnamevar = envtestnamevar

            # get dependencies here
            if test_dict['package-deps'] is not None:
                packages = ' '.join(test_dict['package-deps'])
                log.info('Test:%03d - %s requires: ' % (index, test_dict['testname']))
                log.info(' ----- running install ------')
                rc = os.system("yum -y install %s" % packages)
                if rc != 0:
                    log.info(' ----- install completed - FAILed with rc: %d -----' % rc)
                else:
                    log.info(' ----- install completed ----- %d' % rc)

            # run test here
            index += 1

            rcode = self.run_test(test_dict) # not the rcode of the test, instead this is just 0 or 1 or 2 for timeout
            resultlist.append((index - 1, test_dict['testname'], rcode))

        return resultlist


    def kill_proc(self):
        'function to kill the test process and children when watchdog timer is triggered'

        # create the log file to report with to beaker
        wdlog = '/tmp/watchdogfailure.log'
        os.system('echo "Warning: Test Watch Dog Timer Triggered for testname %s" > %s' % (self.current_testname, wdlog))

        log.info('!!! WARNING: %s has timed out!' % self.current_testname)
        # update beaker entry
        log.info('Reporting the _timeout subresult for testname %s' % self.current_testname)

        # look for even partial behave html report - better then the original wdlog
        if os.path.isfile("/tmp/report_%s.html" % self.envtestnamevar):
            wdlog = "/tmp/report_%s.html" % self.envtestnamevar

        command = 'rhts-report-result %s_timeout %s %s' % (self.envtestnamevar, 'FAIL', wdlog)
        p = subprocess.Popen(command , shell=True)
        rcode = p.wait()
        log.info('report result command: %s rc:%s' % (command, rcode))

        # kill the test process and its children
        log.info('Warning: Test watchdog exceeded time limit, killing test...')
        self.timeouted = True
        os.killpg(self.process.pid, signal.SIGTERM)


    def run_test(self, test):
        self.timeouted = False
        os.chdir(settings['repo_dir'])
        watchdogsec = get_time_seconds(test['timeout'])
        self.current_testname = test['testname']
        # change to the working directory
        if test['dir'] != '.':
            log.info(('Changing dir: %s ' % test['dir']))
            try:
                os.chdir(test['dir'])
            except:
                log.info("Warning: Unexpected error:", sys.exc_info()[0])
                return self.FAIL
        try:
            if '.sh' in test['run']:
                shfile = test['run'].split(' ' , 1)
                log.info('Changing mod to 0775 for %s ' % shfile[0])
                os.chmod(shfile[0], 509)
        except Exception:
            log.info("Warning: Unexpected error: %s", sys.exc_info()[0])
            traceback.print_exc(file=sys.stdout)
            return self.FAIL

        log.info('%s Watchdog: %d seconds ' % (test['run'], watchdogsec))

        # define the log file
        resultlogfile = "result.log"

        # export the testname in /tmp/sth_current_test for ABRT task to pick up
        os.system("echo '%s' > /tmp/sth_current_test" % self.envtestnamevar)
        os.system("chmod 666 /tmp/sth_current_test")

        # execute the defined test, call setpgrp to make the new process a process group leader
        p = subprocess.Popen(test['run'] + ' > ' + resultlogfile + ' 2>&1' , shell=True, preexec_fn=os.setpgrp)

        # save the proc object
        self.process = p

        # config and init watchdogtimer
        t = Timer(watchdogsec, self.kill_proc)

        # start watchdogtimer
        t.start()

        # wait for the test process to return
        rcode = p.wait()

        # remove the file containg the last running test
        if os.path.isfile('/tmp/sth_current_test'):
            os.system('rm -rf /tmp/sth_current_test')

        # cancel watchdogtimer
        t.cancel()

        # read the result log
        rundata = ''
        if os.path.isfile(resultlogfile):
            if sys.version_info.major >= 3:
                with open(resultlogfile, encoding="latin-1") as f:
                    rundata = f.read()
            else:
                with open(resultlogfile, 'r') as f:
                    rundata = f.read()
        else:
            log.info('Warning: Result file does not exist [%s]' % (os.path.join(test['dir'], resultlogfile)))

        # test for the return code to determin pass  / fail
        if p.returncode:
            # bad return code, just log info
            TESTFLAG = False
            log.info('Warning: Failing Test since return code was (%s), while running "%s"' % (p.returncode, test['run']))
            log.info('Test output follows (stderr + stdout joined):')
            for line in rundata.splitlines():
                print(line)

        else:
            # good return code, just check the test log now for exit_codes info
            TESTFLAG = True
            log.info('Info: Test return code was (%s), running "%s"' % (p.returncode, test['run']))
            if settings['debug'].lower() == 'true' or settings['debug'].lower() == 'yes':
                log.info('Test output follows (stderr + stdout joined):')
                for line in rundata.splitlines():
                    print(line)

        if self.timeouted is True:
            log.info('Result: TIMEOUT')
            return self.TIMEOUT
        elif TESTFLAG is True:
            log.info('Result: PASS')
            return self.PASS
        else:
            log.info('Result: FAIL')
            return self.FAIL


def die(why):
    log.info(why)
    sys.exit(-1)


def run():
    read_env_options()
    if not os.path.isdir(clone_dir):
        os.mkdir(clone_dir)
    os.chdir(clone_dir)
    clone_refspec()
    mapper = MapperYaml(get_mapper_yaml(), arches_list, default_plan_id_type)
    install_dependencies(mapper)
    do_setup(mapper)
    tests = filter_tests_to_run(mapper)
    runner = TestRunner(tests)
    results = runner.run_tests()
    do_setup(mapper, cleanup=True)
    log.info('Overall result summary:')
    rc = 0
    outcome = 'PASS'
    for result in results:
        outcome = 'PASS'
        if result[2] == 0:
            pass
        elif result[2] == 1:
            outcome = 'FAIL'
            rc = 1
        elif result[2] == 2:
            outcome = 'TIMEOUT'
            rc = 1
        else:
            outcome = 'UNKNOWN'
            rc = 1
        log.info("Test %d - %s %s" % (result[0], result[1], outcome))
    if len(results) == 0:
        rc = 1
        outcome = 'NO_RESULTS'
    return rc


if __name__ == "__main__":
    sys.exit(run())

sys.exit(0)
