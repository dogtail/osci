#!/bin/bash
set -x

env

# if [ -n "$BEAKER_RECIPE_ID" ]; then
#   ### this can happen only in beaker
#   cp -f ./rhts-report-result /usr/bin/rhts-report-result
#   chmod +x /usr/bin/rhts-report-result
# fi

function collect_test_artifacts ()
{   # Post task - will run at the end of the job
    # Export STH tresults
    TRESULTS=`ls /tmp/tmp.*/tresults.txt`
    if [ -f "$TRESULTS" ]; then
        rhts-submit-log -l $TRESULTS
    fi
    #List abrt crashes
    rpm -q abrt-cli
    if [ $? -eq 0 ]; then
        abrt-cli ls 2>&1 /tmp/abrt.log
        rhts-report-result 'abrt.log' PASS '/tmp/abrt.log'
    fi

    # Packages list
    rpm -qa | sort > /tmp/packages.list
    rhts-report-result "packages.list" PASS "/tmp/packages.list"
    # Journal; do not upload for NetworkManager jobs
    if [ "$COLLECT_JOURNAL" != "no" ]; then
        journalctl -b --no-pager -o short-monotonic | bzip2 > /tmp/journal.log.bz2
        rhts-report-result journal.log.bz2 PASS "/tmp/journal.log.bz2"
    fi

    if [ -f "/tmp/cobertura.xml" ]; then
      rhts-report-result "cobertura.xml" PASS "/tmp/cobertura.xml"
    fi

    if [ -f "/tmp/cc_report.link" ]; then
      rhts-report-result "cc_report.link" PASS "/tmp/cc_report.link"
    fi
}


function set_abrt_autoreport ()
{
    # echo "Refreshing beaker task repo"
    # echo "[beaker-tasks]" > /etc/yum.repos.d/beaker-tasks.repo
    # echo "name=beaker-tasks" >> /etc/yum.repos.d/beaker-tasks.repo
    # echo "baseurl=http://beaker.engineering.redhat.com/repos/$RECIPEID" >> /etc/yum.repos.d/beaker-tasks.repo
    # echo "enabled=1" >> /etc/yum.repos.d/beaker-tasks.repo
    # echo "gpgcheck=0" >> /etc/yum.repos.d/beaker-tasks.repo

    yum -y install python3-lxml # will fail on el7 and el6, but that is of no consequence

    echo ">> Installing mkyral's /distribution/crashes/enable-abrt/"
    yum -y install distribution-distribution-crashes-enable-abrt
    if [ $? -ne 0 ]; then
        echo ">> Failed to install the abrt prep task"
        rhts-report-result $TEST/enable_abrt "FAIL" "/dev/null"
        return 1
    fi

    pushd /mnt/tests/distribution/crashes/enable-abrt/
    echo ">> Setting destination email address to the CI list"
    sed -i 's/SUBMITTER/desktop-qe-ci@redhat.com/' mailx.conf
    make run ; rc=$?
    popd

    RESULT="FAIL"
    if [ $rc -eq 0 ]; then
      RESULT="PASS"
    fi

    rhts-report-result $TEST/enable_abrt $RESULT "/dev/null"
}


function mount_if_not ()
{
    echo "info: checking if /mnt/redhat is mounted"
    mount | grep "/mnt/redhat"
    if [ $? -ne 0 ]; then
        echo "info: mounting /mnt/redhat"
        mkdir /mnt/redhat
        mount -t nfs ntap-bos-c01-eng01-nfs01b.storage.bos.redhat.com:/devops_engineering_nfs/devarchive/redhat /mnt/redhat -o ro,rsize=32768,wsize=8192,bg,auto,noatime,nosuid,nodev,intr
        if [ $? -ne 0 ]; then
            return 1
        fi
        echo "info: /mnt/redhat successfully mounted"
        return 0
    fi
    echo "info: report location already mounted"
}


function set_exp_copr ()
{
    echo "info: EXP_COPR set -> setting the repo"
    return 0 # disable the function

    yum remove -y \*.i686

    #yum -y remove gimp gnome-shell-extensions

    cp ./rhel-7.4-gnome.repo /etc/yum.repos.d/rhel-7.4-gnome.repo
    #cp ./rhel-7.4-candidate.repo /etc/yum.repos.d/rhel-7.4-candidate.repo
    cp ./klember-rhel-7-gnome-3-22-rhel-7.dev.repo /etc/yum.repos.d/klember-rhel-7-gnome-3-22-rhel-7.dev.repo

    yum clean all

    # remove soon
    yum -y install http://download.eng.bos.redhat.com/brewroot/packages/gstreamer1-plugins-bad-free/1.10.4/1.el7/x86_64/gstreamer1-plugins-bad-free-1.10.4-1.el7.x86_64.rpm

    yum -y groupinstall gnome-desktop-environment -x libquadmath* -x xorg-x11* -x *i686 --nogpg --skip-broken

    if [ $? -ne 0 ]; then
        echo "error: Failed to update packages!"
        rhts-report-result $TEST/experimental_copr_gnome_install "FAIL" "/dev/null"
        return 1
    fi
    rhts-report-result $TEST/experimental_copr_gnome_install "PASS" "/dev/null"

    yum -y update -x libquadmath* -x xorg-x11* -x *i686 --nogpg --skip-broken

    echo "info: EXP_COPR set -> attemting to update packages with the COPR"
    if [ $? -ne 0 ]; then
        echo "error: Failed to update packages!"
        rhts-report-result $TEST/experimental_copr_update "FAIL" "/dev/null"
        return 1
    fi
    rhts-report-result $TEST/experimental_copr_update "PASS" "/dev/null"
}


function set_buildroot_repo ()
{
    echo "info: BUILDROOT set -> setting the buildroot-brewtrigger-repo repo"
    mount_if_not
    if [ $? -ne 0 ]; then
        echo "warning: Failed to mount /mnt/redhat"
        echo "warning: Package install might fail on possible dependencies"
        rhts-report-result $TEST/buildroot_repo "FAIL" "/dev/null"
        return 1
    fi
    if [ -d "$BUILDROOT" ]; then
        REPO_PATH="$BUILDROOT"
    else
        echo "warning: The repo does not exist anymore"
        echo "warning: Using the latest repo in the same build target"
        echo "warning:  which may or may not contain possible dependencies"
        REPO_NUMBER=`echo $BUILDROOT |  awk -F \/ '{print $7}'`
        REPO_PATH=`echo $BUILDROOT | sed s/$REPO_NUMBER/latest/`
    fi
    echo "[buildroot-brewtrigger-repo]" > /etc/yum.repos.d/buildroot-brewtrigger-repo.repo
    echo "name=buildroot-brewtrigger-repo" >> /etc/yum.repos.d/buildroot-brewtrigger-repo.repo
    echo "baseurl=file://$REPO_PATH" >> /etc/yum.repos.d/buildroot-brewtrigger-repo.repo
    echo "skip_if_unavailable=1" >> /etc/yum.repos.d/buildroot-brewtrigger-repo.repo
    echo "enabled=1" >> /etc/yum.repos.d/buildroot-brewtrigger-repo.repo
    echo "sslverify=0" >> /etc/yum.repos.d/buildroot-brewtrigger-repo.repo
    echo "gpgcheck=0" >> /etc/yum.repos.d/buildroot-brewtrigger-repo.repo
    echo "priority=888" >> /etc/yum.repos.d/buildroot-brewtrigger-repo.repo
    yum clean all

    echo ${BUILDROOT} | grep rhel-7.3
    if [ $? -eq 0 ]; then
      ## MUST ALSO DELETE ON WORKAROUND REMOVAL
      yum -y install pyatspi at-spi2-core at-spi2-atk
    fi

    rhts-report-result $TEST/buildroot_repo "PASS" "/etc/yum.repos.d/buildroot.repo"
}


function install_packages ()
{   echo ${PACKAGES} | grep /mnt/redhat
    if [ $? -eq 0 ]; then
        mount_if_not
    fi

    echo ">> Installing/updating/downgrading requested packages"
    yum install -y ${PACKAGES} || yum downgrade -y ${PACKAGES}
    if [ $? -ne 0 ]; then
        echo "error: Failed to install packages!"
        rhts-report-result $TEST/package_install "FAIL" "/dev/null"
        return 1
    fi
    rhts-report-result $TEST/package_install "PASS" "/dev/null"
}

function install_brew_builds ()
{
    yum -y install distribution-distribution-install-brew-build
    yum -y install python-lxml
    yum -y install python3-lxml
    export ARCHES="$(arch) noarch"
    export METHOD=install
    pushd /mnt/tests/distribution/install/brew-build
    make run ; rc=$?
    popd

    RESULT="FAIL"
    if [ $rc -eq 0 ]; then
      RESULT="PASS"
    fi
}


function nm_install_packages ()
{   echo ${NM_PACKAGES} | grep /mnt/redhat
    if [ $? -eq 0 ]; then
        mount_if_not
    fi

    echo ">> Hard erasing all installed NM packages"
    rpm -e --all --nodeps $(rpm -qa | grep NetworkManager)

    echo ">> Installing requested packages"
    yum install -y ${NM_PACKAGES}
    if [ $? -ne 0 ]; then
        echo "error: Failed to install packages!"
        rhts-report-result $TEST/package_install "FAIL" "/dev/null"
        return 1
    fi
    rhts-report-result $TEST/package_install "PASS" "/dev/null"
}


function install_deps_yum () # install package deps from setup_def aut_requires_rpms... sth sucks at it
{
    echo ">> Installing packages unless already installed before or by PACKAGES options"
    yum install -y ${INSTALL_DEPS}
    if [ $? -ne 0 ]; then
        echo "warning: Failed to install packages! This may not necessarily be a problem"
        rhts-report-result $TEST/install_deps "FAIL" "/dev/null"
        return 1
    fi
    rhts-report-result $TEST/install_deps "PASS" "/dev/null"
}

function nm_build_id ()
{
    # password used to login on NM jobs... (gets set later too in NM's runtest, this is to make it consistent sooner)
    echo "networkmanager" | passwd --stdin

    if grep -q 'Ootpa' /etc/redhat-release; then
         if [ ! -e /usr/bin/python ]; then
             ln -s /usr/bin/python2 /usr/bin/python
         fi
    fi
    # If we are under RHEL8
    if grep -q 'Ootpa' /etc/redhat-release; then
        # Download script to prepare buildroot repo and run it
        echo ">> Creating buildroot repo file"
        wget --no-check-certificate https://gitlab.cee.redhat.com/desktopqe/desktop-ci/raw/master/openstack/create-buildroot-repo.py \
                -O /tmp/create-buildroot-repo.py
        chmod +x /tmp/create-buildroot-repo.py
        /tmp/./create-buildroot-repo.py /etc/yum.repos.d/rhel-8-buildroot.repo $DISTRO $(arch)
    fi

    echo ">> Getting the build script from upstream"
    wget 'https://gitlab.freedesktop.org/NetworkManager/NetworkManager/raw/automation/contrib/rh-bkr/build-from-source.sh' -O /tmp/nm-build-from-source.sh
    unset WITH_DEBUG
    unset WITH_SANITIZER
    if [ "$NM_BUILD_DEBUG" == 'yes' ]; then
        export WITH_DEBUG=yes
    fi
    if [ "$NM_BUILD_SANITIZER" == 'yes' ]; then
        export WITH_SANITIZER=yes
    fi
    BUILD_ID="$NM_BUILD_ID" BUILD_SNAPSHOT="$NM_BUILD_SNAPSHOT" sh /tmp/nm-build-from-source.sh
    if [ $? -ne 0 ]; then
        # give me back my python
        if grep -q 'Ootpa' /etc/redhat-release; then
             if [ ! -e /usr/bin/python ]; then
                 ln -s /usr/bin/python2 /usr/bin/python
             fi
        fi
        echo "error: Failed to build/install packages from source refspec!"
        rhts-report-result $TEST/build_nm "FAIL" "/dev/null"
        echo "ABORTING THE JOB!"
        rhts-abort -t job
        return 1
    fi
    # give me back my python
    if grep -q 'Ootpa' /etc/redhat-release; then
         if [ ! -e /usr/bin/python ]; then
             ln -s /usr/bin/python2 /usr/bin/python
         fi
    fi
    rhts-report-result $TEST/build_nm "PASS" "/dev/null"
    #packages for export: /tmp/nm-build/NetworkManager/contrib/fedora/rpm/latest/RPMS/$ARCH/
}

function nm_set_debug ()
{
    sed -i -e 's/^ *\(Storage\|SystemMaxUse\|RateLimitBurst\|RateLimitInterval\)=.*/#\0/' \
      -e '$a\Storage=persistent' \
      -e '$a\SystemMaxUse=2G' \
      -e '$a\RateLimitBurst=0/' \
      -e '$a\RateLimitInterval=0/' \
      /etc/systemd/journald.conf ;

    systemctl restart systemd-journald

    mkdir -p /etc/NetworkManager/conf.d/
    cat >/etc/NetworkManager/conf.d/99-test.conf <<EOF
[main]
debug=RLIMIT_CORE,fatal-warnings

[logging]
level=TRACE
domains=ALL
EOF

    rhts-report-result $TEST/nm_debug "PASS" "/dev/null"
}


function nm_valgrind_wrap ()
{
    # Just create the wrapper. Don't enable it or anything; the suppressions
    # are not there yet. The test harness will notice the wrapper is there
    # and will finish the setup.
    sed "s,\(ExecStart=\)\(.*\),\1/bin/bash -c 'G_SLICE=always-malloc G_DEBUG=gc-friendly LOGNAME=root HOSTNAME=localhost exec -a /usr/sbin/NetworkManager /usr/bin/valgrind --num-callers=100 --vgdb=yes --leak-check=full --show-leak-kinds=definite --suppressions=/mnt/tests/NetworkManager-ci/runtime.suppressions --suppressions=$(ls /usr/src/debug/NetworkManager-*/valgrind.suppressions) \2'," \
       </usr/lib/systemd/system/NetworkManager.service \
       >/etc/systemd/system/NetworkManager-valgrind.service

    rhts-report-result $TEST/nm_valgrind "PASS" "/dev/null"
}


function nm_strace_wrap ()
{
	mkdir -p /etc/systemd/system/NetworkManager.service.d
	cat <<EOF > /etc/systemd/system/NetworkManager.service.d/override.conf-strace
[Service]
ExecStart=
ExecStart=/usr/bin/bash -c 'exec strace -f -D -ttt $NM_STRACE_ARGS -o >(bzip2 >> /var/log/NetworkManager-strace.log.bz2) /usr/sbin/NetworkManager --no-daemon'
EOF

    rhts-report-result $TEST/nm_strace "PASS" "/dev/null"
}


function service_restart ()
{   # a XML nicety mainly fo NetworkManager to replace otherwise needed
    # extra /distribution/command
    systemctl daemon-reload
    echo ">> Restarting $RESTART_SERVICE"
    systemctl restart ${RESTART_SERVICE}.service

    /usr/bin/nm-online -s -q --timeout=30
    # sleep 30
    # systemctl restart beah-beaker-backend.service
    # sleep 180
    if [ $? -ne 0 ]; then
        echo "error: service restart did not return 0!"
        rhts-report-result $TEST/restart_service "FAIL" "/dev/null"
        return 1
    fi
    rhts-report-result $TEST/restart_service "PASS" "/dev/null"
}


function redo_console_lock ()
{   # Reboot after install task breaks the console lock, redo that
    echo "Faking a console session..."
    touch /var/run/console/test
    echo test > /var/run/console/console.lock
    rhts-run-simple-test $TEST/fake-console-session "cat /var/run/console/console.lock"
}


function do_temporary_hotfix ()
{   # Reboot after install task breaks the console lock, redo that
    echo "Placeholder function to make temporary hotfixes when stuff is broken..."
    echo "Working around bz1444948 by upgrading the tigervnc to fixed version 7.4 only"
    cat /etc/redhat-release | grep 7.4
    if [ $? -eq 0 ]; then
        yum -y install http://download.eng.bos.redhat.com/brewroot/packages/tigervnc/1.7.90/2.el7/$(arch)/tigervnc-1.7.90-2.el7.$(arch).rpm http://download.eng.bos.redhat.com/brewroot/packages/tigervnc/1.7.90/2.el7/$(arch)/tigervnc-server-1.7.90-2.el7.$(arch).rpm http://download.eng.bos.redhat.com/brewroot/packages/tigervnc/1.7.90/2.el7/$(arch)/tigervnc-server-minimal-1.7.90-2.el7.$(arch).rpm http://download.eng.bos.redhat.com/brewroot/packages/tigervnc/1.7.90/2.el7/$(arch)/tigervnc-server-module-1.7.90-2.el7.$(arch).rpm
        if [ $? -ne 0 ]; then
            echo "error: failed doing the hotfix"
            rhts-report-result $TEST/do_temporary_hotfix_vnc "FAIL" "/dev/null"
            return 1
        fi
        rhts-report-result $TEST/do_temporary_hotfix_vnc "PASS" "/dev/null"
        # yum -y install http://download.eng.bos.redhat.com/brewroot/packages/mutter/3.22.3/7.el7/$(arch)/mutter-3.22.3-7.el7.$(arch).rpm
        # if [ $? -ne 0 ]; then
        #     echo "error: failed doing the hotfix"
        #     rhts-report-result $TEST/do_temporary_hotfix_vnc "FAIL" "/dev/null"
        #     return 1
        # fi
        # rhts-report-result $TEST/do_temporary_hotfix_vnc "PASS" "/dev/null"
    fi
    # There are still some crashes in latest eng-ops composes. This should fix most of them
    if grep -q '7.5' /etc/redhat-release; then
        yum update -y \
        http://download.eng.bos.redhat.com/brewroot/packages/mutter/3.26.2/4.el7/$(uname -p)/mutter-3.26.2-4.el7.$(uname -p).rpm \
        http://download.eng.bos.redhat.com/brewroot/packages/gtk3/3.22.26/1.el7/$(uname -p)/gtk3-3.22.26-1.el7.$(uname -p).rpm \
        http://download.eng.bos.redhat.com/brewroot/packages/gtk3/3.22.26/1.el7/$(uname -p)/gtk3-immodule-xim-3.22.26-1.el7.$(uname -p).rpm \
        http://download.eng.bos.redhat.com/brewroot/packages/libgweather/3.26.0/1.el7/$(uname -p)/libgweather-3.26.0-1.el7.$(uname -p).rpm \
        http://download.eng.bos.redhat.com/brewroot/packages/flatpak/0.8.8/2.el7/$(uname -p)/flatpak-0.8.8-2.el7.$(uname -p).rpm \
        http://download.eng.bos.redhat.com/brewroot/packages/flatpak/0.8.8/2.el7/$(uname -p)/flatpak-libs-0.8.8-2.el7.$(uname -p).rpm
    fi

    # in early beta composes of RHEL8 /usr/bin/python is missing
    if grep -q 'Ootpa' /etc/redhat-release; then
         if [ ! -e /usr/bin/python ]; then
             ln -s /usr/bin/python2 /usr/bin/python
         fi
    fi
}

if [ "$COLLECT" == "yes" ]; then
    collect_test_artifacts
    exit 0
fi
if [ -n "$ABRT" ]; then
    set_abrt_autoreport
fi
if [ -n "$EXP_COPR" ]; then
    set_exp_copr
fi

if [ -n "$BUILDROOT" ]; then
    set_buildroot_repo
fi
if [ -n "$PACKAGES" ]; then
    install_packages
fi
if [ -n "$BREW_BUILDS" ]; then
    #export BREW_CMD=download-build
    export BUILDS=$BREW_BUILDS
    install_brew_builds
fi
if [ -n "$BREW_TASKS" ]; then
    #export BREW_CMD=download-task
    export TASKS=$BREW_TASKS
    install_brew_builds
fi
if [ -n "$NM_PACKAGES" ]; then
    nm_install_packages
fi
if [ -n "$NM_BUILD_ID" ]; then
    nm_build_id
fi
if [ "$NM_DEBUG" == "yes" ]; then
    nm_set_debug
fi
if [ "$NM_VALGRIND" == "yes" ]; then
    nm_valgrind_wrap
elif [ "$NM_STRACE" == "yes" ]; then
    nm_strace_wrap
fi
if [ -n "$INSTALL_DEPS" ]; then
    install_deps_yum
fi
if [ "$REDO_CONSOLE_LOCK" == "yes" ]; then
    redo_console_lock
fi

do_temporary_hotfix

if [ -n "$RESTART_SERVICE" ]; then
    service_restart
fi

rhts-report-result $TEST "PASS" "/dev/null"
exit 0
