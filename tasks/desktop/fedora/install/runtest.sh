#!/bin/bash
. /usr/bin/rhts-environment.sh

set -x

# add the user in case it does not exist
useradd test

#set the root and test password to 'redhat' (for overcoming polkit easily)
echo "Setting root password to 'redhat'"
echo "redhat" | passwd root --stdin
echo "Setting test password to 'redhat'" # just in case
echo "redhat" | passwd test --stdin

#fake console
echo "Faking a console session..."
touch /var/run/console/test
echo test > /var/run/console/console.lock

#passwordless sudo
echo "enabling passwordless sudo"
if [ -e /etc/sudoers.bak ]; then
    mv -f /etc/sudoers.bak /etc/sudoers
fi
cp -a /etc/sudoers /etc/sudoers.bak
grep -v requiretty /etc/sudoers.bak > /etc/sudoers
echo 'Defaults:test !env_reset' >> /etc/sudoers
echo 'test ALL=(ALL)   NOPASSWD: ALL' >> /etc/sudoers

#setting ulimit to unlimited for test user
echo "ulimit -c unlimited" >> /home/test/.bashrc

#installation of desktop stuff
rhts-run-simple-test $TEST "grep test /etc/sudoers"
rhts-run-simple-test $TEST/user-to-video-grp "usermod -a -G video test"
rhts-run-simple-test $TEST/user-to-wheel-grp "usermod -a -G wheel test"

# sometimes rawhide repos can contain DB in newer version than one used in the original image we're spawning from
cat /etc/redhat-release | grep 'Fedora'
if [ $? -eq 0 ]; then
  dnf clean dbcache
fi

#mkdir -p /usr/lib64/python2.7/site-packages/dogtail_gui_helper
#cp __init__.py gnome_apps_helper.py kde_apps_helper.py -t /usr/lib64/python2.7/site-packages/dogtail_gui_helper/

#if [ "$SKIP_BROKEN" == "yes" ]; then
#    SKIP="--skip-broken"
#fi

if [ "x$DESKTOP" == "xKDE" ]; then
    echo "export QT_ACCESSIBILITY=1" >> /home/test/.bashrc
    rhts-run-simple-test $TEST/kde-desktop-environment "dnf -y groupinstall kde-desktop-environment" #KDE
    rhts-run-simple-test $TEST/qt-at-spi "dnf -y localinstall qt-at-spi"
elif [ "x$DESKTOP" == "xMINIMAL" ]; then
    rhts-run-simple-test $TEST/gnome-desktop-minimal "dnf -y install gdm gnome-shell gnome-session gnome-classic-session at-spi2-atk" #GNOME
else
    rhts-run-simple-test $TEST/workstation-product-environment "dnf -y groupinstall workstation-product-environment --allowerasing"
    rhts-run-simple-test $TEST/basic-desktop-environment "dnf -y groupinstall basic-desktop-environment --allowerasing"
fi

# Enable the dummy driver if this machine is headless.
# All video outputs available on the system are represented as directories
# under /sys/class/drm.  Each of them has the status file containing either
# "connected" or "disconnected".

# # NOTE: theres not such file on f25... + dummy driver broken + bz1393114
# hostname | grep 'qe-dell-ovs'
# if [ $? -ne 0 ]; then
#     #     echo "enabling dummy/void configuration for real hw machine"
#     # wird, ey?
#     /bin/cp xorg.conf /etc/X11
# fi
# VNC is super usefull anyhoo and this works without dummy driver too
/bin/cp 10-dummylibvnc.conf /etc/X11/xorg.conf.d

rhts-run-simple-test $TEST/basics "dnf -y --nogpg install git nano wget"

rhts-run-simple-test $TEST/x-server "dnf -y install xorg-x11-server-Xorg xorg-x11-drivers xorg-x11-server-Xvfb tigervnc-server tigervnc-server-module wpa_supplicant xterm"

/bin/cp ofourdan-ponytail-fedora-32.repo /etc/yum.repos.d/
/bin/cp dogtail.repo /etc/yum.repos.d/

rhts-run-simple-test $TEST/x11vnc "dnf -y --nogpg install x11vnc"
rhts-run-simple-test $TEST/dogtail-behave "dnf -y --nogpg install python3-dogtail python3-dogtail-scripts python3-behave gnome-ponytail-daemon-python --skip-broken"

ln -s /usr/bin/behave /usr/bin/behave-3 # bunch of our tests use this to run behave

# patched version of headless that has hotfix for gnome-shell env being unreadable from normal user on F32
# also longer waits after binary actvated as we are still wating for gnome-shell but stealing from gvfs
#cp -f ./dogtail-run-headless-next /usr/bin/dogtail-run-headless-next

# git is not preinstalled, if /distribution/install is ommited
rhts-run-simple-test $TEST/git "dnf -y install git abrt-python"

rhts-run-simple-test $TEST/other-deps "dnf -y install python-pip python3-pip python3-pexpect --skip-broken"

rhts-run-simple-test $TEST/html-formatter "pip-3 install behave-html-formatter"

cp ./behave.ini /home/test

if [ -e /usr/share/xsessions/1-kde-plasma-standard.desktop ]; then
    echo "making legacy symlink of kde-plasma session file"
    ln -s /usr/share/xsessions/1-kde-plasma-standard.desktop /usr/share/xsessions/kde-plasma.desktop
fi

# preinstall packages for abrt autoreporting task - as some dep issues may happen with errata prep
# dont make it report as subtest... we don't care too much if this fails
#yum -y install abrt abrt-addon-ccpp libreport-plugin-logger libreport-plugin-ureport libreport-plugin-reportuploader libreport-plugin-mailx

# put in /etc/profile.d/, /etc/kde/env/
cp qt-at-spi.sh /etc/profile.d/
mkdir -p /etc/kde/env/
cp qt-at-spi.sh /etc/kde/env/

#install brewgrab
rhts-run-simple-test $TEST/brewgrab "/bin/cp brewgrab.py /usr/bin/brewgrab"

#FORCE usage of Xorg on fedora
sed -i 's/#WaylandEnable=false/WaylandEnable=false/' /etc/gdm/custom.conf

sudo -u test touch ~/.config/gnome-initial-setup-done

service gdm stop

pkill -9 gnome-shell
pkill -9 X
pkill -9 gdm

sleep 1

# ps aux

sudo -u test dogtail-run-headless-next --force-xorg ./first_session.py
#
sleep 5
#
sudo -u test dogtail-run-headless-next --force-xorg ./first_session.py
#
# exit 0
