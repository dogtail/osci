#!/bin/python
from time import sleep
import os

sleep(20)

os.system('ps aux')

os.system('gsettings set org.gnome.desktop.interface toolkit-accessibility true')

sleep(1)

os.system('gsettings set org.gnome.desktop.interface toolkit-accessibility true')

sleep(5)

try:
    from dogtail.tree import root
    print([x.name for x in root.children])
except Exception as e:
    print(e)

from dogtail.rawinput import keyCombo, typeText, pressKey
from dogtail.config import config

config.typingDelay = 0.2

keyCombo('<Alt>F2')
sleep(3)
typeText("gsettings set org.gnome.desktop.interface toolkit-accessibility true")
pressKey('enter')
sleep(1)

os.system('gnome-screenshot')
os.system('rhts-report-result /first_session "PASS" "/dev/null"')
