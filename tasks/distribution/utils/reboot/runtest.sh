#!/bin/bash

. /usr/bin/rhts_environment.sh
 
if [[ ${REBOOTCOUNT} == 0 ]]; then 
	rhts-reboot
else
	report_result ${TEST} PASS 0
fi


exit 0

