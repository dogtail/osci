#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /distribution/crashes/enable-abrt
#   Description: Install and enable ABRT
#   Author: Richard Marko <rmarko@redhat.com>
#           Martin Kyral <mkyral@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012, 2016 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh

PACKAGE="crashes"

rlJournalStart
    rlPhaseStartTest
        if ! [ -f /tmp/disable-qe-abrt ] ; then
            rlLog "Installing required packages"
            if [ -f /usr/bin/dnf ] ; then
               PKGMGMT=dnf
            else
               PKGMGMT=yum
            fi
            rlRun "$PKGMGMT -y --skip-broken install abrt abrt-addon-ccpp libreport-plugin-logger libreport-plugin-ureport libreport-plugin-reportuploader libreport-plugin-mailx"

            if rlIsRHEL 6 ; then
                RHEL='rhel6'
            elif rlIsRHEL 7 ; then
                RHEL='rhel7'
            elif rlIsRHEL 8 ; then
                RHEL='rhel8'
            fi

            if rlIsRHEL 6.6; then
                # fetch repository
                rlRun "wget --timeout=15 -O /etc/yum.repos.d/$RHEL-staging.repo http://abrt.brq.redhat.com/repo/$RHEL-staging/$RHEL-staging.repo"
                # update abrt & libreport
                rlRun "yum update --enablerepo=$RHEL-staging -y 'abrt*' 'libreport*'"
            fi

            # add entry in hosts
            rlRun "cp abrt_for_qe.conf /etc/libreport/events.d/"
            # process crashes in unsigned packages
            rlRun "sed -i 's/OpenGPGCheck.*=.*yes/OpenGPGCheck = no/' /etc/abrt/abrt-action-save-package-data.conf"
            rlRun "cp mailx.conf /etc/libreport/plugins/mailx-qe.conf"
            rlIsRHEL 6 && rlRun "patch /etc/libreport/events.d/abrt_event.conf disable_sos.patch"
            rlIsRHEL 7 && rlRun "patch /etc/libreport/events.d/abrt_event.conf disable_sos.patch"

            SUBMITTER=${SUBMITTER:-$(grep SUBMITTER /var/beah/* 2>/dev/null | grep -v Binary | sed 's/.*SUBMITTER": "//' | sed 's/",.*//' | sort | uniq)}
            if [ -n "$SUBMITTER" ] ; then
                rlRun "sed -i 's/SUBMITTER/$SUBMITTER/' /etc/libreport/plugins/mailx-qe.conf"
            elif [ -n "$RSTRNT_OWNER" ] ; then
                rlRun "sed -i 's/SUBMITTER/$RSTRNT_OWNER/' /etc/libreport/plugins/mailx-qe.conf"
            fi

            rlLog "Starting abrtd"
            rlServiceEnable abrtd
            rlServiceStart abrtd
            rlLog "Starting abrt-ccpp"
            rlServiceEnable abrt-ccpp
            rlServiceStart abrt-ccpp
            rlLog "Starting abrt-oops"
            rlServiceEnable abrt-oops
            rlServiceStart abrt-oops
            rlLog "Starting abrt-vmcore"
            rlServiceEnable abrt-vmcore
            rlServiceStart abrt-vmcore
            if ls /usr/lib/systemd/system/abrt-journal-core.service &>/dev/null ; then
                rlLog "Starting abrt-journal-core"
                rlServiceEnable abrt-journal-core
                rlServiceStart abrt-journal-core
            fi
        else
            rlLog "Disabled by presence of /tmp/disable-qe-abrt"
            rlLog "Nothing to do"
        fi
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
