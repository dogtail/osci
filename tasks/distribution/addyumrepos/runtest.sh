#!/bin/sh

# Source the common test script helpers
. /usr/bin/rhts_environment.sh

function CreateYumRepoConfigs ()
{
    echo "Enter CreateYumRepoConfigs" | tee -a $OUTPUTFILE

    for repo in $(env | grep -i ^repourl); do
        reponame=$(echo $repo | sed 's/[rR][eE][pP][oO][uU][rR][lL]\([^=]\)=.*/\1/')
        repourl=$(echo $repo | sed "s/[rR][eE][pP][oO][uU][rR][lL]$reponame=//")
        echo "Creating repo name: addyumrepo$reponame, url: $repourl" | tee -a $OUTPUTFILE
        cat << EOF >/etc/yum.repos.d/addyumrepo$reponame.repo
[addyumrepo$reponame]
name=addyumrepo$reponame
baseurl=$repourl
enabled=1
gpgcheck=0
skip_if_unavailable=1
EOF
    done

    ls -la /etc/yum.repos.d/ | tee -a $OUTPUTFILE
    echo "Exit CreateYumRepoConfigs" | tee -a $OUTPUTFILE
}

# create yum repo config files from REPOURL* env. variables
CreateYumRepoConfigs

report_result $TEST PASS 0
exit 0
