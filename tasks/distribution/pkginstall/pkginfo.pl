#!/usr/bin/perl
@arches=("i386","i586","i686","x86_64","ia64","s390","s390x","noarch","ppc","ppc64","src");
 $arch=$variant="";

$pkgvr = $ARGV[0];

# check for http://,ftp:// packages...
if ($pkgvr =~ /^(ftp:|http:)\/\//) {

	@pkg_arr=split(/\//, $ARGV[0]);
	$pkgvr = pop(@pkg_arr);
	$pkgvr =~ s/\.rpm$//g;

}

# start from the end..
foreach $thearch (@arches) { 
   if ($pkgvr =~ /($thearch)$/) { 
      $arch=$thearch;  
      #print "arch is : $arch \n";
      $pkgvr =~ s/\.($thearch)$//g ;
      #print "pkgvr is : $pkgvr \n";
   }
} 

$pkg = $pkgvr;   


if ( $pkgvr =~ /.*-(\d+.*?-\d+.*)$/ ) { # we have VR
   $vr = $1;
   $pkg = $pkgvr;
   #$pkg =~ s/-$vr//;
   $pkg =~ s/-\d.*//;
   #print "vr is : $vr \n";
   #print "the pkg is : $pkg \n";

} elsif ( $pkgvr =~ /.*-(\d+.*?)$/ ) {
   $vr = $1;
   $pkg = $pkgvr;
   #$pkg =~ s/-$vr//;
   $pkg =~ s/-\d.*//;
}

if ( $pkg =~ /^kernel-rt/ ) {
   $pkg =~ /^kernel-rt-(.*$)/;
   $pkgbase = "kernel-rt";
   $variant = "rt";
} elsif ( $pkg =~ /^kernel-xen/ ) {
   $pkg =~ /^kernel-xen-(.*$)/;
   $pkgbase = "kernel-xen";
   $variant = "xen";
} elsif ( $pkg =~ /^kernel-utils/ ) {
   $pkg =~ /^kernel-utils-(.*$)/;
   $pkgbase = "kernel-utils";
   $variant = $1;
} elsif ( $pkg =~ /^kernel/ ) {
   $pkg =~ /^kernel-(.*$)/;
   $pkgbase = "kernel";
   $variant = $1;
} 

if ( ! $pkgbase ) {
   # This is an attempt for the impossible solution. Packages have their
   # variants such as -utils, -headers, -common, -debuginfo, etc. For example,
   # for glibc package there are glibc-utils, glibc-headers, etc. variants.
   # however, there are no restrictions or naming conventions from a package
   # having names ending with those suffixes, especially in the case of -utils
   # such as nfs-utils, bluez-utils etc. So for now, we'll take any package
   # name that ends with -utils as a whole package name rather than as a
   # variant. However, this is broken and might not be fixed at all.. maybe
   # querying brewroot/koji etc here could be used to determine for sure?
 
   if ( $pkg =~ /(.*)-(\w+)$/ || $pkg =~ /(.*)-(\w+)-(\w+)$/ ) { 
     if ( $2 eq "common" || $2 eq "debuginfo" || $2 eq "devel" || \
          $2 eq "headers" || $2 eq "debug" ) {
        $pkgbase=$1; 
     } else {
	$pkgbase=$pkg;
     }
   } else { 
     $pkgbase = $pkg; 
   }
} 

if ( $vr =~ /(.*)-(.*)/) 
{ 
   $ver=$1; 
   $rel=$2; 
} else {
   $ver=$vr;
}
 
print "$pkgbase:$pkg:$variant:$ver:$rel:$arch \n"; 
exit 0;
