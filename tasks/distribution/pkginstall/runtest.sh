#!/bin/sh

# This script will install packages with specific versions from the repos
# available to the test. The user needs to ensure that the yum repo that
# contains the specific versions of the packages needs to be available to the
# test environment. A yum repo can be added to the test with -A argument
#
# After all the packages installed/upgraded, the script will reboot. If the
# package is a kernel package, then it'll make sure to select that kernel as the
# default kernel before it reboots.
#

#set -x
# Source the common test script helpers
. /usr/bin/rhts_environment.sh

# control where to log debug messages to:
# devnull = 1 : log to /dev/null
# devnull = 0 : log to file specified in ${DEBUGLOG}
devnull=0

# Create debug log
DEBUGLOG=`mktemp -p /mnt/testarea -t pkginstall.XXXXXX`

# locking to avoid races
lck=$OUTPUTDIR/$(basename $0).lck

## to keep track of packages whose installations failed/passed
FAILED=0
PASSED=0
# Functions

# Log a message to the ${DEBUGLOG} or to /dev/null
function DeBug ()
{
    local msg="$1"
    local timestamp=$(date +%Y%m%d-%H%M%S)
    if [ "$devnull" = "0" ]; then
	lockfile -r 1 $lck
	if [ "$?" = "0" ]; then
	    echo -n "${timestamp}: " >>$DEBUGLOG 2>&1
	    echo "${msg}" >>$DEBUGLOG 2>&1
	    rm -f $lck >/dev/null 2>&1
	fi
    else
	echo "${msg}" >/dev/null 2>&1
    fi
}

function RprtRslt ()
{
    ONE=$1
    TWO=$2
    THREE=$3

    # File the results in the database
    report_result $ONE $TWO $THREE

    if [ "$TWO" == "FAIL" ]; then
	SubmitLog $DEBUGLOG
    fi
}

function SubmitLog ()
{
    LOG=$1
    rhts_submit_log -l $LOG
}

function SelectKernel ()
{
    DeBug "Enter SelectKernel"
    VR=$1
    EXTRA=$2
    DeBug "VR=$VR EXTRA=$EXTRA"

    # If not version or Extra selected then choose the latest installed version
    if [ -z "$EXTRA" -a -z "$VR" ]; then
	DeBug "ERROR: missing args"
	return 1
    fi

    # Workaround for broken RT kernel spec file, part 1
    if [ "$EXTRA" = "rt" ]; then
	DeBug "EXTRA=$EXTRA"
	EXTRA=""
    fi

    # Workaround for broken RT kernel spec file, part 2
    if [ "$EXTRA" = "rt-vanilla" ]; then
	DeBug "EXTRA=$EXTRA"
	EXTRA="vanilla"
    fi

    # Workaround for broken RT kernel spec file, part 1
    if [ "$EXTRA" = "up" ]; then
	DeBug "EXTRA=$EXTRA"
	EXTRA=""
    fi

    echo "***** Attempting to switch boot kernel to ($VR$EXTRA) *****" | tee -a $OUTPUTFILE
    DeBug "Attempting to switch boot kernel to ($VR$EXTRA)"

    grub_file=/boot/grub/grub.conf

    if [ -f $grub_file ]; then
	DeBug "Using: $grub_file"
	COUNT=0
	DEFAULT=undefined
	for i in $(grep '^title' $grub_file | sed 's/.*(\(.*\)).*/\1/'); do
	    DeBug "COUNT=$COUNT VR=$VR EXTRA=$EXTRA i=$i"
	    if echo $i | egrep -e "${VR}.*${EXTRA}" ; then
		DEFAULT=$COUNT;
	    fi
	    COUNT=$(expr $COUNT + 1)
	done
	if [[ x"${DEFAULT}" != x"undefined" ]]; then
	    DeBug "DEFAULT=$DEFAULT"
	    /bin/ed -s $grub_file <<EOF
/default/
d
i
default=$DEFAULT
.
w
q
EOF
	fi
	DeBug "$grub_file"
	cat $grub_file | tee -a $DEBUGLOG
    fi

    elilo_file=/boot/efi/efi/redhat/elilo.conf

    if [ -f $elilo_file ]; then
	DeBug "Using: $elilo_file"
	DEFAULT=$(grep -A 2 "image=vmlinuz-$VR$EXTRA$" $elilo_file | awk -F= '/label=/ {print $2}')
	DeBug "DEFAULT=$DEFAULT"
	if [ -n "$DEFAULT" ]; then
	    DeBug "DEFAULT=$DEFAULT"
	    /bin/ed -s $elilo_file <<EOF
/default/
d
i
default=$DEFAULT
.
w
q
EOF
	fi
	DeBug "$elilo_file"
	cat $elilo_file | tee -a $DEBUGLOG
    fi

    yaboot_file=/boot/etc/yaboot.conf

    if [ -f $yaboot_file ] ; then
	DeBug "Using: $yaboot_file"
	grep vmlinuz $yaboot_file
	if [ $? -eq 0 ] ; then
	    VM=z
	else
	    VM=x
	fi
	DEFAULT=$(grep -A 1 "image=/vmlinu$VM-$VR$EXTRA" $yaboot_file | awk -F= '/label=/ {print $2}')
	DeBug "DEFAULT=$DEFAULT"
	if [ -n "$DEFAULT" ] ; then
	    sed -i 's/label=linux/label=orig-linux/g' $yaboot_file
	    sed -i 's/label='$DEFAULT'/label=linux/g' $yaboot_file
	    DeBug "DEFAULT=$DEFAULT"
	    grep -q label=linux $yaboot_file
	    if [ $? -ne 0 ] ; then
		sed -i 's/label=orig-linux/label=linux/g' $yaboot_file
		DeBug "Reverted back to original kernel"
	    fi
	fi
	DeBug "$yaboot_file"
	cat $yaboot_file | tee -a $DEBUGLOG
    fi

    zipl_file=/etc/zipl.conf

    if [ -f $zipl_file ] ; then
	DeBug "Using: $zipl_file"
	DEFAULT=$(grep "image=/boot/vmlinuz-$VR$EXTRA" $zipl_file | awk -Fvmlinuz- '/vmlinuz/ {printf "%.15s\n",$2}')
	DeBug "DEFAULT=$DEFAULT"
	if [ -n "$DEFAULT" ] ; then
	    DeBug "$VR$EXTRA"
	    tag=$(grep "\[$DEFAULT\]" $zipl_file)
	    DeBug "tag=$tag"
	    if [ -z "$tag" ] ; then
		DeBug "Setting it back to default"
		DEFAULT=linux
	    fi
	    /bin/ed -s $zipl_file <<EOF
/default=/
d
i
default=$DEFAULT
.
w
q
EOF
	    zipl
	fi
	DeBug "$zipl_file"
	cat $zipl_file | tee -a $DEBUGLOG
    fi

    sync
    sleep 5
    DeBug "Exit SelectKernel"
    return 0
}

function YumInstallPackage ()
{

    # this function will yum install the package given.. As much information of
    # the package name given will be used. If only package name is given then
    # only the package name will be passed to yum.. if version/release/arch are
    # given they'll be passed on too..
    #
    unset SUFFIX YUMCMD pkg2Binstalled
    if [[ -n ${pkgver} ]]; then
	SUFFIX="-${pkgver}"
    fi
    if [[ -n ${pkgrel} ]]; then
	SUFFIX=${SUFFIX}"-${pkgrel}"
    fi
    if [[ -n ${pkgarch} ]]; then
	SUFFIX=${SUFFIX}".${pkgarch}"
    fi	
    pkg2Binstalled="${pkgname}${SUFFIX}"

    if rpm -q ${pkg2Binstalled}; then
        YUMCMD="yum -y upgrade"
    else
        YUMCMD="yum -y install"
    fi

    # kernel is different...
    if echo ${package} | egrep  "^\s*kernel"; then
	YUMCMD="yum -y install"
	pkg2Binstalled="${package}"
    fi

	echo "pkg2Binstalled : $pkg2Binstalled "
    # Check and see if the kernel available for install from a repo
    DeBug "Enter YumInstallPackage"
    DeBug "YUMCMD= $YUMCMD $pkg2Binstalled $pkgdev2Binstalled $pkgheader2Binstalled $pkgdebug2Binstalled"
    echo "***** Install package via $YUMCMD $pkg2Binstalled *****" | tee -a $OUTPUTFILE
    DeBug "$YUMCMD $pkg2Binstalled"

    if yum list available $pkg2Binstalled; then
        $YUMCMD $pkg2Binstalled
        # skip test for installed package if '*' is [in] package name (e.g. python27*)
        if [[ ! "$pkg2Binstalled" == *\* ]]; then
            if ! rpm -q $pkg2Binstalled; then
                echo "***** Yum was unsuccessful with:  $YUMCMD $pkg2Binstalled *****" | tee -a $OUTPUTFILE
                DeBug "Exit YumInstallPackage FAIL 3 (YUM exited with a failure)"
                let "FAILED=${FAILED}+1"
                return 3
            else
                RprtRslt $TEST/YumInstallPackage_${pkg2Binstalled} PASS 0
                return 0
            fi
        fi
    else
        echo "*** $pkg2Binstalled doesn't seem to exist in any yum repos .. ***" | tee -a $OUTPUTFILE
        return 4
    fi

    DeBug "Exit YumInstallPackage SUCCESS"
    return 0
}

function BrewInstallPackage ()
{
    # Is the package available for install from a BREW
    DeBug "Enter BrewInstallPackage"
    DeBug "$httpbase/$pkgarch/$pkgbase.$pkgarch.rpm"
    DeBug "***** Install $pkgbase via BREW $pkgbase.$pkgarch.rpm *****"
    ## if pkg is installed, then do upgrade, if not then do install
    if rpm -q $pkgname; then
	if echo $pkg | egrep -q '^kernel'; then
		echo "$pkgname exists on the system and is installing" | tee -a $OUTPUTFILE
		RPMCMD="rpm -ivh "
	else
		echo "$pkgname exists on the system and is not kernel..  Upgrading" | tee -a $OUTPUTFILE
		RPMCMD="rpm -Uvh "
	fi
    else
	echo "$pkgname doesn't exist on the system..  installing" | tee -a $OUTPUTFILE
	RPMCMD="rpm -ivh "
    fi

    $RPMCMD $httpbase/$pkgarch/$pkgbase.$pkgarch.rpm
    if [ "$?" -ne "0" ]; then
	DeBug "Forcing the rpm command --force"
	$RPMCMD --force $httpbase/$pkgarch/$pkgbase.$pkgarch.rpm
	if [ "$?" -ne "0" ]; then
	    DeBug "Exit BrewInstallPackage FAIL 6"
	    let "FAILED=${FAILED}+1"
	    return 6
	fi
    else
	RprtRslt $TEST/BrewInstallPackage_$httpbase/$pkgarch/$pkgbase.$pkgarch.rpm PASS 0
	rpm -q $pkgbase
    fi

    for thepkg in $pkgdevel $pkgdebug $pkgheader; do
	if wget --spider $httpbase/$pkgarch/${thepkg}.${pkgarch}.rpm; then
	    	echo "installing $thepkg from BREW " | tee -a $OUTPUTFILE
    		$RPMCMD $httpbase/$pkgarch/${thepkg}.${pkgarch}.rpm
		if [[ $? != 0 ]]; then
			DeBug "$RPMCMD $httpbase/$pkgarch/${thepkg}.${pkgarch}.rpm failed .."
		else
			RprtRslt $TEST/BrewInstallPackage_$httpbase/$pkgarch/${thepkg}.${pkgarch}.rpm PASS 0
		fi
	fi
    done
    DeBug "Exit BrewInstallPackage SUCCESS"
    return 0
}

## function InstallSourceRpm
#  first downloads the file and then install.
function InstallSourceRpm ()
{
   local srcpackage;

   if [[ $# != 1 ]]; then
	echo "No package arg is given to InstallSourceRpm"
	return 1
   fi
   srcpackage=$1

   yumdownloader --source ${srcpackage}
   if [[ $? != 0 ]]; then
	echo "problem with yumdownloader --source ${srcpackage}"
	return 1
   fi

   rpm -ivh ${pkgname}*.rpm
   if [[ $? != 0 ]]; then
	echo "problem with rpm -ivh ./${pkgname}*.rpm"
	return 1
   fi

   return 0

} # end of InstallSourceRpm

# function to extract V-R-A ..
# returns pkgbase:pkgname:variant:version:release:arch:
function getinfo ()
{

    local package
    if [[ $# != 1 ]]; then
	echo " Need a package to work on...";
	exit 1;
    fi
    DeBug "package in getinfo is: $1"

    ./pkginfo.pl $1

} # end of function getinfo

function Main ()
{
	DeBug "Enter Main"
	REBOOT=no

	if [[ -n "${PKGGRPNAME}" ]]; then
		eval yum -y groupinstall ${PKGGRPNAME}
	fi
	# check to make sure that we have names/versions in synch

	for package in $PKGARGNAME; do
		DeBug "package is : $package"
		echo "***** Start of $package install test *****" | tee -a $OUTPUTFILE

		#get the package info:
		pkginfo=$(getinfo $package)
		pkg=$(echo $pkginfo | awk -F':' '{print $1}')
		pkgname=$(echo $pkginfo | awk -F':' '{print $2}')
		pkgvariant=$(echo $pkginfo | awk -F':' '{print $3}')
		pkgver=$(echo $pkginfo | awk -F':' '{print $4}')
		pkgrel=$(echo $pkginfo | awk -F':' '{print $5}')
		pkgarch=$(echo $pkginfo | awk -F':' '{print $6}')
		VR=${pkgver}-${pkgrel}
		echo "pkginfo: $pkginfo pkg: $pkg pkgname: $pkgname pkgvariant: $pkgvariant pkgver: $pkgver pkgrel: $pkgrel pkgarch: $pkgarch"

		### packages can be given as a http/ftp locations to be directly installed
		# from those locations... we'll take of them here...
		if echo $package | egrep -q '^(ftp|http)://'; then
			# get the package name...
			pkgname=$(echo $package | awk -F'/' '{print $NF}')
			if echo $pkgname | egrep -q '^kernel'; then
				rpm -ivh $package
			else
				rpm -Uvh $package
			fi

			if [[ $? != 0 ]]; then
				echo "Error installing $package .. exiting";
				RprtRslt $TEST/InstallPackage_${package} FAIL 1
				let "FAILED=${FAILED}+1"
				continue
			else
				RprtRslt $TEST/InstallPackage_${package} PASS 0
			fi

			# if it's kernel we gotta make sure that the package is selected to be
			# booted from and reboot option is set to yes..
			if echo $pkgname | egrep -q '^kernel'; then
				REBOOT=yes
				SelectKernel ${pkgver}-${pkgrel} ${pkgvariant}
			fi # end of if echo $pkgname | egrep -q '^kernel'; then
			continue;
		fi # end of if echo $package | egrep -q '^(ftp|http)://'; then

		# if this is a source package, handle it.
		if [[ x"${pkgarch}" == "xsrc" ]]; then
			mkdir -p tmpdir_src
			srcpkg=${package%.$pkgarch}
			InstallSourceRpm ${srcpkg}
			if [[ $? != 0 ]]; then
				echo "Installing ${srcpkg} failed."
				RprtRslt $TEST/InstallPackage_${srcpkg} FAIL 1
				let "FAILED=${FAILED}+1"
				continue
			else
				RprtRslt $TEST/InstallPackage_${srcpkg} PASS 0
				continue
			fi
		fi # end of if [[ x"${pkgarch}" == "xsrc" ]];

		# make sure that we at least have the NVR
		DeBug "pkg: $pkg ; pkgname: $pkgname ; pkgvariant: $pkgvariant ; pkgver: $pkgver ; pkgrel : $pkgrel ; pkgarch : $pkgarch "
		if echo $package | egrep '^kernel' | egrep -q -v "debug|devel|headers|utils"; then
			# if no arch is given append the correct one
			if [[ -z $pkgarch ]]; then
				package=${package}.${kernarch}
				pkgarch=${kernarch}
			fi

			if [[ -z ${pkgvariant} ]]; then
				pkgbase=${pkgname}-${VR}
				pkguname=${VR}
				DeBug "Test package install variables"
				DeBug "1=$pkgbase 2=$pkgname 3=$pkgver 4=$pkgrel 5=$pkgvariant "
			else
				DeBug "Running up variant, or newer smp"
				pkgbase=${pkgname}-${VR}
				pkguname=${VR}${pkgvariant}
				DeBug "Test package install variables"
				DeBug "1=$pkgbase 2=$pkgname 3=$pkgver 4=$pkgrel 5=$pkgvariant "
			fi


			if [[  x"${pkgver}" == "x" ]]; then
				echo "For the kernel package you MUST specify a version" | tee -a $OUTPUTFILE
				RprtRslt $TEST/InstallKernelSyntax FAIL 1
				let "FAILED=${FAILED}+1"
				exit 0
			fi

			if [[  x"${pkgrel}" == "x" ]]; then
				echo "For the kernel package you MUST specify a release" | tee -a $OUTPUTFILE
				RprtRslt $TEST/InstallKernelSyntax FAIL 1
				let "FAILED=${FAILED}+1"
				exit 0
			fi

			# if we install a kernel we need to make sure that we'll reboot the system.
			REBOOT=yes

			httpbase=http://porkchop.redhat.com/brewroot/packages/$pkg/$pkgver/$pkgrel

			# Check to see if the kernel we want to test is already running
			runkernel=$(/bin/uname -r)
			DeBug "runkernel=$runkernel"
			if [ "$pkguname" != "$runkernel" ]; then
				# Check to see if the kernel we want to test is already installed
				rpm -qa --queryformat '%{name}-%{version}-%{release}.%{arch}\n' | grep -q $pkgbase.$pkgarch
				if [ "$?" -ne "0" ]; then
					# Install kernel variant from yum repo
					YumInstallPackage
					if [ "$?" -ne "0" ]; then
						echo "***** Could not install $pkgname from yum repo trying *****" | tee -a $OUTPUTFILE
						# Install from yum failed lets try direct from brew
						BrewInstallPackage
						if [ "$?" -ne "0" ]; then
							echo "***** Could not install $pkgname from brew either" | tee -a $OUTPUTFILE
							RprtRslt $TEST/BrewInstallPackage FAIL $?
							let "FAILED=${FAILED}+1"
							exit 0
						fi
					fi
				fi

				SelectKernel ${pkgver}-${pkgrel} ${pkgvariant}
				if [ "$?" -ne "0" ]; then
					RprtRslt $TEST/SelectKernel FAIL $?
					let "FAILED=${FAILED}+1"
				fi

				echo "***** End of kernel install test *****" | tee -a $OUTPUTFILE
				if [ -f $OUTPUTDIR/boot.$kernbase ]; then
					SubmitLog $OUTPUTDIR/boot.$kernbase
				fi
				SubmitLog $DEBUGLOG
			else
				echo "***** The running kernel is the kernel we want to test *****" | tee -a $OUTPUTFILE
				echo "***** End of kernel install test *****" | tee -a $OUTPUTFILE
				if [ -f $OUTPUTDIR/boot.$kernbase ]; then
					SubmitLog $OUTPUTDIR/boot.$kernbase
				fi
				RprtRslt $TEST/$kernbase PASS $REBOOTCOUNT
			fi

		else # this is not a kernel package:
			# pkgbase will be pkgname only if there is no version-release info available:
			if [[ "is_set${VR}" == "is_set" ]]; then
				pkgbase=${pkgname}
			else
				pkgbase=${pkgname}-${VR}
			fi

			pkgbase=${pkgbase%\.${pkgarch}}
			DeBug "Test package install variables"
			DeBug "1=$pkgbase 2=$pkgname 3=$pkgver 4=$pkgrel "
			# check to see if the package has been installed first..
			if [[ -n ${pkgver} ]]; then
				if [[ -n ${pkgrel} ]]; then
					# -devel and -headers needs to be trimmed if they exist
					pkg=$(echo "${pkg%-headers}")
					pkg=$(echo "${pkg%-devel}")
					pkg=$(echo "${pkg%-debuginfo}")
					httpbase=http://porkchop.redhat.com/brewroot/packages/$pkg/$pkgver/$pkgrel
					rpm -q ${pkgbase}-${pkgver}-${pkgrel}
					if [ "$?" -eq "0" ]; then
						echo "**** ${pkgbase}-${pkgver}-${pkgrel} is already installed ***" | tee -a $OUTPUTFILE
						continue;
					fi
				else
					rpm -q ${pkgbase}-${pkgver}
					if [ "$?" -eq "0" ]; then
						echo "**** ${pkgbase}-${pkgver} is already installed ***" | tee -a $OUTPUTFILE
						continue;
					fi
				fi
			fi

			#if there is no version/release etc. then we can only do a yum install
			if [[ -z $pkgrel ]];  then
				YumInstallPackage
				rc=$?
				if [[ $rc != 0 ]]; then
					# if the package is already installed, then don't
					# report failure...
					if rpm -q ${package}; then
						echo "${package} is already installed in the system..." | tee -a $OUTPUTFILE
						RprtRslt $TEST/${package}_AlreadyInstalled PASS 0
					else
						echo "***** Could not install $pkg from yum repo" | tee -a $OUTPUTFILE
						RprtRslt $TEST/YumInstallPackage FAIL $rc
						let "FAILED=${FAILED}+1"
						exit 0
					fi
				fi
			else
				# try to install pkg from yum repo first
				YumInstallPackage
				if [ "$?" -ne "0" ]; then
					if ! rpm -q ${package}; then
						echo "***** Could not install $pkg from yum repo *****" | tee -a $OUTPUTFILE
						# Install from yum failed lets try direct from brew
						if [[ -n ${pkgver} && -n ${pkgrel} ]]; then
							if [[ -z ${pkgarch} ]]; then
								#default the arch to kernel's arch
								pkgarch=${kernarch}
							fi
							DeBug " got pkgver=${pkgver} and pkgrel=${pkgrel} and pkgarch=${pkgarch} for pkg=${pkg}"
							BrewInstallPackage
							rc=$?
							if [[ $rc != 0 ]]; then
								echo "***** Could not install $pkgname from brew either" | tee -a $OUTPUTFILE
								RprtRslt $TEST/BrewInstallPackage FAIL $?
								let "FAILED=${FAILED}+1"
								exit 0
							 else
								echo "***** Installed $pkgname from brew " | tee -a $OUTPUTFILE
							 fi
						else
							 echo "Can't do brewinstall without n-v-r.arch" | tee -a $OUTPUTFILE
							 RprtRslt $TEST/pkginstall_$package FAIL 1
							 let "FAILED=${FAILED}+1"
							 exit 0
						fi
					fi
				fi
			fi

			# if package is not kernel and VERLOCK is set
			[[ -n "$VERLOCK" ]] && {
				yum install -y yum-plugin-versionlock &>/dev/null
				echo "{debug} yum versionlock ${pkgbase}"
				yum versionlock ${pkgbase}
			}
		fi

		unset pkginfo pkg pkgname pkgvariant pkgver pkgrel pkgarch VR pkgbase
	done
}

# Record the test version in the debuglog
testver=$(rpm -qf $0)
DeBug "$testver"

# Current kernel variables
kernbase=$(rpm -q --queryformat '%{name}-%{version}-%{release}\n' -qf /boot/config-$(uname -r))
kername=$(rpm -q --queryformat '%{name}\n' -qf /boot/config-$(uname -r))
kernver=$(rpm -q --queryformat '%{version}\n' -qf /boot/config-$(uname -r))
kernrel=$(rpm -q --queryformat '%{release}\n' -qf /boot/config-$(uname -r))
kernarch=$(rpm -q --queryformat '%{arch}\n' -qf /boot/config-$(uname -r))
kernvariant=$(uname -r | sed -e "s/${kernver}-${kernrel}//g")
kerndevel=$(rpm -q --queryformat '%{name}-devel-%{version}-%{release}\n' -qf /boot/config-$(uname -r))
DeBug "Running kernel variables"
DeBug "1=$kernbase 2=$kername 3=$kernver 4=$kernrel 5=$kernarch 6=$kernvariant 7=$kerndevel"

# Test ARGS that are defined
DeBug "TEST ARGS (Environment variables)"
DeBug "PKGARGNAME=$PKGARGNAME"
DeBug "PKGGRPNAME=$PKGARGNAME"

if [ -z "$OUTPUTDIR" ]; then
	OUTPUTDIR=/mnt/testarea
fi

# Record the boot messages
/bin/dmesg > $OUTPUTDIR/boot.$kernbase
if [ ! -s $OUTPUTDIR/boot.$kernbase ]; then
	# Workaround for /distribution/install zeroing out the dmesg file
	cp $OUTPUTDIR/boot.messages $OUTPUTDIR/boot.$kernbase
fi

if [[ "x${PKGARGNAME}" == "x" && "x${PKGGRPNAME}" == "x" ]]; then
	echo "***** Test argument(s) are empty! Can't continue. *****" | tee -a $OUTPUTFILE
	echo "***** either PKGARGNAME or PKGGRPNAME must be given *****" | tee -a $OUTPUTFILE
	RprtRslt $TEST/pkginstall FAIL 1
	let "FAILED=${FAILED}+1"
fi

if [[ ${REBOOTCOUNT} == 0 ]]; then
	Main

	if [ -f $OUTPUTDIR/boot.$kernbase ]; then
		SubmitLog $OUTPUTDIR/boot.$kernbase
	fi

	#if there were any kernel packages installed, then reboot.
	if [[ x"${REBOOT}" == xyes ]]; then
		rhts-reboot
	fi

	if [[ ${FAILED} == 0 ]]; then
		RprtRslt $TEST/pkginstall PASS $REBOOTCOUNT
	else
		RprtRslt $TEST/pkginstall FAIL ${FAILED}
	fi
	SubmitLog $DEBUGLOG
else
	RprtRslt $TEST/rhts-reboot PASS 0
	fi
exit 0
#end
