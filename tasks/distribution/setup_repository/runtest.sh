#!/bin/sh

. /usr/bin/rhts_environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

test_string="./test.py"

if rlIsRHEL ">=8"; then
  sed -i 's|^#!/usr/bin/python|#!/usr/libexec/platform-python|' $test_string
fi


# Check for preset command line argument
if [ -n "$preset" ]
then test_string="$test_string --preset $preset"
fi
# Check for repo_name command line argument
if [ -n "$repo_name" ]
then test_string="$test_string --repo_name $repo_name"
fi
# Check for name command line argument
if [ -n "$name" ]
then test_string="$test_string --name $name"
fi
# Check for url command line argument
if [ -n "$url" ]
then test_string="$test_string --url $url"
fi
# Check for gpg_key_url command line argument
if [ -n "$gpg_key_url" ]
then test_string="$test_string --gpg_key_url $gpg_key_url"
fi
# Check for disable_gpg_check command line argument
if [ -n "$disable_gpg_check" ]
then test_string="$test_string --disable_gpg_check"
fi
# Check for priority command line argument
if [ -n "$priority" ]
then test_string="$test_string --priority $priority"
fi
# Check for disable_repo command line argument
if [ -n "$disable_repo" ]
then test_string="$test_string --disable_repo"
fi

rhts-run-simple-test $TEST "$test_string"

rhts-submit-log -l ./setup_repository.log
