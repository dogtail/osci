#!/usr/bin/python
# Copyright (c) 2014 Red Hat, Inc. All rights reserved. This copyrighted material
# is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General
# Public License v.2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Author: Ken Benoit

__author__ = 'Ken Benoit'

import logging
import logging.config
import optparse
import time
import xml.etree.ElementTree
import subprocess
import re

class setup_repository(object):
    """
    setup_repository class contains all the necessary methods to create a custom
    repository on the test system.

    """
    def __init__(self):
        self.__logger = None
        self.__option_parser = optparse.OptionParser()
        self.__log_file_name = 'setup_repository.log'
        self.__presets_file_name = './presets.xml'
        self.__repository_file_location = "/etc/yum.repos.d"
        self.__configure_command_line_options()

    def __configure_command_line_options(self):
        option_parser = self.get_command_line_option_parser()

        # Define the dry run command line option
        option_parser.add_option(
            '-d',
            '--dry_run',
            dest = 'dry_run',
            action = 'store_true',
            default = False,
            help = 'put the test into dry run mode where the repository file '
                + 'will be displayed but will not be written to the system',
        )
        # Define the preset command line option
        option_parser.add_option(
            '-p',
            '--preset',
            dest = 'preset',
            action = 'store',
            default = None,
            help = 'name of a preset defined in the presets.xml file to use as '
                + 'the definition to create the repository file from',
        )
        # Define the repo_name command line option
        option_parser.add_option(
            '-r',
            '--repo_name',
            dest = 'repo_name',
            action = 'store',
            default = None,
            help = 'name to use for the .repo file and the name to use in the '
                + '[] section. If not supplied then the time in milliseconds '
                + 'since epoch will be used',
        )
        # Define the name command line option
        option_parser.add_option(
            '-n',
            '--name',
            dest = 'name',
            action = 'store',
            default = None,
            help = 'descriptive name to use in the .repo file',
        )
        # Define the url command line option
        option_parser.add_option(
            '-u',
            '--url',
            dest = 'url',
            action = 'store',
            default = None,
            help = 'URL to use in the .repo file',
        )
        # Define the gpg_key_url command line option
        option_parser.add_option(
            '-k',
            '--gpg_key_url',
            dest = 'gpg_key_url',
            action = 'store',
            default = None,
            help = 'URL to the GPG key to import for the repository',
        )
        # Define the disable_gpg_check command line option
        option_parser.add_option(
            '--disable_gpg_check',
            dest = 'disable_gpg_check',
            action = 'store_true',
            default = False,
            help = 'if set then the GPG check will be disabled for the '
                + 'repository',
        )
        # Define the priority command line option
        option_parser.add_option(
            '--priority',
            dest = 'priority',
            action = 'store',
            default = None,
            help = 'if set the repository will have specified priority'
        )
        # Define the disable_repo command line option
        option_parser.add_option(
            '--disable_repo',
            dest = 'disable_repo',
            action = 'store_true',
            default = False,
            help = 'if set then the repository will be disabled',
        )

    def get_command_line_option_parser(self):
        """
        Get the command line option parser.

        Return value:
        OptionParser object.

        """
        return self.__option_parser

    def get_logger(self):
        """
        Get the logger object.

        Return value:
        logging.Logger object.

        """
        if not self.__logger:
            self.__logger = logging.getLogger('root')
            self.__logger.setLevel(logging.DEBUG)
            file_handler = logging.FileHandler(
                filename = self.__log_file_name,
                mode = 'w',
            )
            file_handler.setLevel(logging.DEBUG)
            console_handler = logging.StreamHandler()
            console_handler.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(asctime)s %(levelname)s - '
                + '%(module)s %(funcName)s line %(lineno)s: %(message)s')
            file_handler.setFormatter(formatter)
            console_handler.setFormatter(formatter)
            self.__logger.addHandler(file_handler)
            self.__logger.addHandler(console_handler)
        return self.__logger

    def write_repo_file(
        self,
        repo_name = int(time.time() * 1000),
        name = None,
        url = None,
        disable_gpg_check = False,
        disable_repo = False,
        priority = None,
        dry_run = False,
        ):
        """
        Write out the repository file.

        Keyword arguments:
        repo_name - Name of the repository file and the name to use in the []
                    section within the file.
        name - Descriptive name to use for the repository.
        url - URL to the repository.
        disable_gpg_check - If True then the GPG check will be disabled for the
                            repository.
        priority - priority of the repository
        disable_repo - If True then the repository will be disabled.
        dry_run - If True then the file contents will be displayed but the file
                  will not be written.

        """
        # Get the logger
        logger = self.get_logger()

        file_name = "{repo_dir}/{repo_name}.repo".format(
            repo_dir = self.__repository_file_location,
            repo_name = repo_name,
        )
        file_contents = []
        file_contents.append("[{repo_name}]".format(repo_name = repo_name))
        file_contents.append("name={name}".format(name = name))
        file_contents.append("baseurl={url}".format(url = url))
        if disable_gpg_check:
            file_contents.append("gpgcheck=0")
        if priority:
            file_contents.append("priority={priority}".format(priority = priority))
        if disable_repo:
            file_contents.append("enabled=0")
        logger.info("Writing file {file_name}:\n{file_contents}".format(
                file_name = file_name,
                file_contents = "\n".join(file_contents),
            )
        )
        # Write out the actual file
        if not dry_run:
            self.run_command(
                command = 'echo "{line}" > {file_name}'.format(
                    line = file_contents[0],
                    file_name = file_name,
                ),
            )
            for line in file_contents[1:]:
                self.run_command(
                    command = 'echo "{line}" >> {file_name}'.format(
                        line = line,
                        file_name = file_name,
                    ),
                )

    def import_gpg_key(self, gpg_key_url, dry_run):
        """
        Import the GPG key.

        Keyword arguments:
        gpg_key_url - URL to the GPG key.
        dry_run - If True then the GPG key will not actually be imported.

        """
        # Get the logger
        logger = self.get_logger()

        logger.info("Importing GPG key {url}".format(
                url = gpg_key_url,
            )
        )
        # Import the GPG key
        if not dry_run:
            self.run_command(
                command = 'rpm --import {url}'.format(
                url = gpg_key_url,
                )
            )

    def run_command(self, command):
        """
        Run the command provided on the system.

        Keyword arguments:
        command - Command to run.

        Return value:
        List of command output.

        """
        logger = self.get_logger()
        logger.debug("Running command: {0}".format(command))
        process = subprocess.Popen(
            command,
            shell = True,
            stdout = subprocess.PIPE,
            stderr = subprocess.STDOUT,
        )
        process.wait()
        output = process.stdout.readlines()
        output_str_lines = []
        for line in output:
            if type(line) is not str:
                line = line.decode("utf-8")
            output_str_lines.append(line)
        logger.debug("Command output: {0}".format("".join(output_str_lines)))
        return output_str_lines

    def get_preset_values(self, preset, xml_file = None):
        """
        Parse the presets XML file and return the correct values for the system
        being run on.

        Keyword arguments:
        preset - Name of the preset to get the values from.
        xml_file - Path to the presets XML file.

        Return value:
        Dictionary containing values for the repository file specific for the
        system being run on.

        """
        # Get the logger
        logger = self.get_logger()
        if not xml_file:
            xml_file = self.__presets_file_name
        repo_settings = {
            'repo_name': None,
            'name': None,
            'url': None,
            'gpg_key_url': None,
            'disable_gpg_check': None,
            'priority': None,
            'disable_repo': None,
        }
        setting_names = repo_settings.keys()
        # Sub section traversal information
        sub_sections = {
            'structure': {
                'os': {
                    'structure': {
                        'major_version': {
                            'structure': {
                                'minor_version': {
                                    'structure': {
                                        'variant': {
                                            'structure': {
                                                'arch': {
                                                    'structure': {},
                                                    'order': [],
                                                    'attribute': 'type',
                                                },
                                            },
                                            'order': [
                                                'arch',
                                            ],
                                            'attribute': 'name',
                                        },
                                        'arch': {
                                            'structure': {},
                                            'order': [],
                                            'attribute': 'type',
                                        },
                                    },
                                    'order': [
                                        'variant',
                                        'arch',
                                    ],
                                    'attribute': 'version',
                                },
                                'variant': {
                                    'structure': {
                                        'arch': {
                                            'structure': {},
                                            'order': [],
                                            'attribute': 'type',
                                        },
                                    },
                                    'order': [
                                        'arch',
                                    ],
                                    'attribute': 'name',
                                },
                                'arch': {
                                    'structure': {},
                                    'order': [],
                                    'attribute': 'type',
                                },
                            },
                            'order': [
                                'minor_version',
                                'variant',
                                'arch',
                            ],
                            'attribute': 'version',
                        },
                        'variant': {
                            'structure': {
                                'arch': {
                                    'structure': {},
                                    'order': [],
                                    'attribute': 'type',
                                },
                            },
                            'order': [
                                'arch',
                            ],
                            'attribute': 'name',
                        },
                        'arch': {
                            'structure': {},
                            'order': [],
                            'attribute': 'type',
                        },
                    },
                    'order': [
                        'major_version',
                        'variant',
                        'arch',
                    ],
                    'attribute': 'name',
                },
            },
            'order': [
                'os',
            ],
            'attribute': None,
        }
        # Determine what OS and architecture we're running on
        known_os_releases = {
            'Red Hat Enterprise Linux': [
                'Server',
                'Workstation',
                'Client',
                'ComputeNode',
            ],
            'Fedora': [],
        }
        system_information = {
            'os': None,
            'major_version': None,
            'minor_version': None,
            'variant': None,
            'arch': None,
        }
        # Run through the commands to determine the OS
        os_release_commands = ['cat /etc/redhat-release', 'cat /etc/issue']
        for command in os_release_commands:
            output = self.run_command(command = command)
            for line in output:
                for (os, variants) in known_os_releases.items():
                    if re.search(os, line):
                        system_information['os'] = os
                        for variant in variants:
                            if re.search(variant, line):
                                system_information['variant'] = variant
                                break
                            if system_information['variant']:
                                break
                        match = re.search('release (?P<major_version>\d+)\.?(?P<minor_version>\d*)', line)
                        if match:
                            match_dict = match.groupdict()
                            if 'major_version' in match_dict:
                                system_information['major_version'] = match_dict['major_version']
                            if 'minor_version' in match_dict:
                                system_information['minor_version'] = match_dict['minor_version']
                        if system_information['os']:
                            break
                    if system_information['os']:
                        break
                if system_information['os']:
                    break
            if system_information['os']:
                break
        # Run through the commands to determine the architecture
        arch_type_commands = ['uname -i']
        for command in arch_type_commands:
            output = self.run_command(command = command)
            if output[0] != "":
                system_information['arch'] = output[0].strip()
        logger.debug(
            "System information:\n"
            + "OS: {0}\n".format(system_information['os'])
            + "Major version: {0}\n".format(system_information['major_version'])
            + "Minor version: {0}\n".format(system_information['minor_version'])
            + "Variant: {0}\n".format(system_information['variant'])
            + "Architecture: {0}\n".format(system_information['arch'])
        )
        # Parse the presets XML file
        tree = xml.etree.ElementTree.parse(xml_file)
        root = tree.getroot()
        if root.tag != 'presets':
            raise Exception("Unable to find the presets tag in {0}".format(
                xml_file
                )
            )
        # Find the requested preset
        found_preset = None
        for preset_node in root:
            if preset_node.attrib['name'] == preset:
                found_preset = preset_node
                break
        if found_preset is None:
            raise Exception(
                'Unable to find preset "{preset}" in {preset_file}'.format(
                    preset = preset,
                    preset_file = xml_file,
                )
            )
        # Traverse the preset for the settings
        settings = self._traverse_node(
            node = found_preset,
            sub_sections = sub_sections,
            setting_names = setting_names,
            system_information = system_information,
        )
        for name in settings.keys():
            repo_settings[name] = settings[name]
        return repo_settings

    def _traverse_node(self, node, sub_sections, setting_names, system_information):
        """
        Traverse an XML node to find any repository settings.

        Keyword arguments:
        node - XML element to traverse.
        sub_sections - Dictionary defining the traversal instructions.
        setting_names - List of names of the settings for entries to make in the
                        repository file.
        system_information - Dictionary of information of the system being run
                             on to aid in repository settings selection.

        Return value:
        Dictionary of repository setting values.

        """
        logger = self.get_logger()
        settings = {}
        for entry in node:
            # Pull out settings first
            for setting_name in setting_names:
                if entry.tag == setting_name:
                    settings[setting_name] = entry.text
            # Traverse through sub sections
            for section_name in sub_sections['order']:
                # See if the sub section in priority order matches the name of
                # current tag
                if entry.tag == section_name:
                    # Check to see if the sub section attribute exists in the
                    # current tag
                    if sub_sections['structure'][section_name]['attribute'] in entry.attrib:
                        # Check to see if the pertinent system information
                        # matches the current tag's attribute
                        if entry.attrib[sub_sections['structure'][section_name]['attribute']] == system_information[section_name]:
                            node_settings = self._traverse_node(
                                node = entry,
                                sub_sections = sub_sections['structure'][section_name],
                                setting_names = setting_names,
                                system_information = system_information,
                            )
                            # Copy the repository settings found from traversal
                            # of the sub section
                            for name in node_settings.keys():
                                settings[name] = node_settings[name]
                            # Since we had a match, bounce out early so the
                            # repository settings from the traversal are not
                            # overwritten
                            return settings
        return settings

    def run_test(self):
        """
        Run the test to create the repository file.

        Return value:
        Integer exit code indicating the result of the test run.

        """
        # Parse the command line options
        (options, args) = self.get_command_line_option_parser().parse_args()

        # Get the logger
        logger = self.get_logger()

        dry_run = options.dry_run

        repo_settings = {
            'repo_name': None,
            'name': None,
            'url': None,
            'gpg_key_url': None,
            'disable_gpg_check': None,
            'priority': None,
            'disable_repo': None,
        }
        bindings = {
            'repo_name': options.repo_name,
            'name': options.name,
            'url': options.url,
            'gpg_key_url': options.gpg_key_url,
            'disable_gpg_check': options.disable_gpg_check,
            'priority': options.priority,
            'disable_repo': options.disable_repo,
        }

        # If given a preset get the correct values
        if options.preset:
            settings = self.get_preset_values(preset = options.preset)
            for name in repo_settings.keys():
                repo_settings[name] = settings[name]
        # If we were given specific repository values on the command line then
        # use them over any other values
        for name in repo_settings.keys():
            if bindings[name]:
                repo_settings[name] = bindings[name]

        # Write the repository file to the system
        self.write_repo_file(
            repo_name = repo_settings['repo_name'],
            name = repo_settings['name'],
            url = repo_settings['url'],
            disable_gpg_check = repo_settings['disable_gpg_check'],
            priority = repo_settings['priority'],
            disable_repo = repo_settings['disable_repo'],
            dry_run = dry_run,
        )

        # Import the GPG key (if provided)
        if repo_settings['gpg_key_url']:
            self.import_gpg_key(
                gpg_key_url = repo_settings['gpg_key_url'],
                dry_run = dry_run,
            )

        return 0

if __name__ == '__main__':
    exit(setup_repository().run_test())
