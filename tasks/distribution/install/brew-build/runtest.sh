#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /distribution/install/brew-build
#   Description: updates system with packages from Brew
#   Author: Karel Srot <ksrot@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
[ -e /usr/bin/rhts-environment.sh ] && . /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh

# global variables / arguments
# BUILDS - builds to download and install - e.g. "aide-0.14-5.el6_2 rsyslog-5.8.10-7.el6"
# TASKS - tasks IDs (instead of specifying BUILD) - e.g. "6196542". In case of parent tasks all childs are processed
# ARCHES - architecture of the requested packages, the default is src + all archs installed on the system
# METHOD - localinstall/localupdate/install/update - yum/dnf command for installing packages
# PARAMS - additional command line arguments passed to yum/dnf
# SERVER - Brew build server to fetch packages from (brew|koji), default "brew"
# RPM_BLACKLIST - egrep regular expression used for filtering rpms/packages not to install, e.g. "(foo|bar)"
# CREATEREPO - package and tool name of createrepo command to use, this can be different in RHEL versions
# CURL_OPTIONS - additional curl options
# VERSIONLOCK - true/false - configure versionlock plugin to lock to required RMS.

[ -n "$METHOD" ] || METHOD=update
[ -n "$PARAMS" ] || PARAMS=""

[ "$VERSIONLOCK" == "true" ] || VERSIONLOCK="false"
if rlIsRHEL 4; then
  # rhel-4 doesn't support rlGetPrimaryArch function
  [ -n "$ARCHES" ] || ARCHES=`rpm -qa --qf '%{ARCH}\n' | sort -u | grep -v none`
else
  [ -n "$ARCHES" ] || ARCHES="noarch $(rlGetPrimaryArch) $(rlGetSecondaryArch)"
fi
echo "$ARCHES" | grep -q 'i686' && ARCHES="$ARCHES i386"
echo $SERVER | grep -qi koji && SERVER_URL=https://koji.fedoraproject.org/koji || SERVER_URL=https://brewweb.engineering.redhat.com/brew

# package manager
rlIsRHEL '<=7' && PKG_MGR="yum" || PKG_MGR="dnf"

# use --best with dnf in case of install to also upgrade packages - NOT NEEDED AS THIS IS A DEFAULT IN RHEL-8
#[ "$PKG_MGR" = "dnf" -a $METHOD = "install" ] && METHOD="--best install"

ARCHES=`echo $ARCHES | sed 's/ /|/g'`   # replace spaces with |

if rlIsRHEL '>7' || rlIsFedora; then
    CREATEREPO="createrepo_c"
else
    CREATEREPO="createrepo"
fi

# curl does not fallback from GSS to noauth if auth fails since v6.64.1
function curl_cmd() {
    URL="$1"
    curl --negotiate -u : -k --location-trusted $CURL_OPTIONS "$URL" 2> /dev/null \
        || curl -u : -k --location-trusted $CURL_OPTIONS "$URL" 2> /dev/null
}

# for given src package (e.g. rsyslog-5.8.10-7.el6) and architecture gets brew download links for subpackages
# another option is to pass a buildID, e.g. 283343
function get_pkg_list_for_build() {
  PKG=$1
  ARCH="$2"
  if echo $PKG | egrep -q '^[0-9]+$'; then  # buildID
      curl_cmd "$SERVER_URL/buildinfo?buildID=$PKG" | grep '>download</a>' | egrep "($ARCH|src)\.rpm" | egrep -o 'http[s]?://[^"]*'
  else  # build
      # escaping '+' from build names (e.g. for RHEL-8 modules)
      PKG_QUERY=$(sed -e 's:+:%2B:g' <<< $PKG)
      curl_cmd "$SERVER_URL/search?match=exact&type=build&terms=$PKG_QUERY" | grep '>download</a>' | egrep "($ARCH|src)\.rpm" | egrep -o 'http[s]?://[^"]*'
  fi
}

# for given taskID prints URL for build RPMs
# for parent task process all childs
function get_pkg_list_for_task() {
    ID=$1
    ARCH=$2
    TMPFILE=`mktemp`
    curl_cmd "$SERVER_URL/taskinfo?taskID=$ID" > $TMPFILE
    # check if the task is a parent task...
    if grep -q '<span class="treeLabel">' $TMPFILE; then
        # for parent task we need to process all childs
        CHILDS=`grep -A 2 '<span class="treeLabel">' $TMPFILE | egrep -o 'taskID=[0-9]+' | cut -d '=' -f 2`
        rm $TMPFILE
        # now process each child
        for CHILD in $CHILDS; do
            get_pkg_list_for_task $CHILD $ARCH
        done
    else  # it is not a parent task 
        egrep "($ARCH|src)\.rpm</a>" $TMPFILE | egrep -o 'http[s]?://[^"]*'
        rm $TMPFILE
    fi
}

# checks if required packages are present in the required versions
function check_pkg_versions_installed() {
    RET=0
    CNT=0
    for PKGFILE in $@; do
        PNAME=`rpm -q --queryformat '%{NAME}.%{ARCH}' -p $PKGFILE`
        if rlIsRHEL '<7'; then
            PNEVRA=`rpm -q --queryformat '%{NVRA}' -p $PKGFILE`  # rpm bug on RHEL-6, rpm -q NEVRA doesn't work correctly
        else
            PNEVRA=`rpm -q --queryformat '%{NEVRA}' -p $PKGFILE`
        fi
	if rpm -q $PNAME &>/dev/null; then
            CNT=$(( $CNT+1 ))
            if ! rpm -q $PNEVRA &>/dev/null; then
              echo "$PNAME is present on the system but $PNEVRA is not installed"
              RET=$(( $RET+1 ))
            fi
        fi
    done
    if [ $CNT -eq 0 ] && ! echo $METHOD | egrep -qi '(update|upgrade)'; then  # no packages present and we were not just updating
      rlLogInfo "None of the packages are installed on the system after an update."
      return 99
    else
      return $RET
    fi
}


rlJournalStart
    rlPhaseStartSetup
        for RPM in "$CREATEREPO"; do
            if ! rlCheckRpm $RPM; then
                if rlIsRHEL 4; then  # there is no createrepo in RHEL4
                  rlRun "wget http://download.eng.bos.redhat.com/brewroot/packages/createrepo/0.4.4/6/noarch/createrepo-0.4.4-6.noarch.rpm"
                  rlRun "rpm -ivh createrepo-0.4.4-6.noarch.rpm"
                  rm createrepo-0.4.4-6.noarch.rpm
                else
                  rlRun "$PKG_MGR -y install $RPM"
                fi
            fi
            rlAssertRpm $RPM || rlDie "Required rpm $RPM is not installed"
        done
	#rm -rf /var/tmp/$REPONAME
	#rlRun "mkdir -p /var/tmp/$REPONAME" 0 "Creating repo directory /var/tmp/$REPONAME"
	rlRun "REPODIR=\`mktemp -d -p /var/tmp brew-build-repo-XXX\`"
    rlRun "chmod go+rx $REPODIR"
	REPONAME=`basename $REPODIR`
	pushd $REPODIR
    rlPhaseEnd

    rlPhaseStartTest "Build installation"
	rlLog "Using server $SERVER_URL"
	if [ -n "$BUILDS$TASKS" ]; then  # I have BUILDS or TASKS specified
            for BUILD in $BUILDS; do
	        rlLog "Downloading RPMs for build $BUILD"
	        rlRun "get_pkg_list_for_build $BUILD '$ARCHES' | sed 's/%2B/+/g' | tee RPMS.list"
                rlAssertGreater "RPM list should not be empty" `cat RPMS.list | wc -l` 0
                echo "--- RPM list ---"
                cat RPMS.list
                echo "----------------"
	        rlRun "wget --no-verbose -i RPMS.list" 
                cat RPMS.list >> allRPMS.list  # gather RPM list across multiple brew builds
	    done
            for TASK in $TASKS; do
	        rlLog "Downloading RPMs for task $TASK"
	        rlRun "get_pkg_list_for_task $TASK '$ARCHES' | sed 's/%2B/+/g' | tee RPMS.list"
                rlAssertGreater "RPM list should not be empty" `cat RPMS.list | wc -l` 0
                echo "--- RPM list ---"
                cat RPMS.list
                echo "----------------"
	        rlRun "wget --no-verbose -i RPMS.list" 
                cat RPMS.list >> allRPMS.list  # gather RPM list across multiple brew builds
	    done
	    rm RPMS.list
	    ls -l *.rpm
	    rlRun "rpm -qp *.rpm" 0 "Checking that all downloaded files are RPM packages"
	    if [ $? -eq 0 ]; then
	        rlRun "$CREATEREPO ." 0 "Creating repo metadata"
	        rlRun "cat > /etc/yum.repos.d/$REPONAME.repo <<EOF
[$REPONAME]
name=$REPONAME
baseurl=file://$REPODIR
gpgcheck=0
enabled=1
EOF" 0 "Setting up repo $REPONAME"
	        cat /etc/yum.repos.d/$REPONAME.repo
	        rlRun "$PKG_MGR --disablerepo=\* --enablerepo=$REPONAME makecache" 0 "Refresh package manager cache"
	    #rlRun "ls | grep -v '.src.rpm' | xargs yum -y $METHOD" 0 "Running yum $METHOD on downloaded packages"
	        PACKAGES=`rpm -q --queryformat '%{NAME} ' -p *.rpm`
                PNVRAS=`rpm -q --queryformat '%{NVRA} ' -p *.rpm`
                if [ -n "$RPM_BLACKLIST" ]; then
	          RPMLIST=`ls | egrep -v '(src.rpm|repodata|RPMS.list)' | egrep -v "$RPM_BLACKLIST" | xargs echo`
                  PACKAGES=`echo $PACKAGES | tr ' ' '\n' | egrep -v "$RPM_BLACKLIST" | xargs echo`
                else
	          RPMLIST=`ls | egrep -v '(src.rpm|repodata|RPMS.list)' | xargs echo`
                fi

	        if echo $METHOD | grep -q multi; then # try multiple methods in sequence
		    RPMS=`echo $RPMLIST | sed -e "s#\([^ ]*\)#$REPODIR\/\1#g"`
		    # remove duplicit rpms downloaded, like *.noarch.rpm.1
	            RPMS=`echo $RPMS | tr ' ' '\n' | grep -v "rpm\.[0-9*]$" | tr '\n' ' '`
		    # remove 32b rpms and debuginfo because of conflicts
		    if [ "${DISABLE_SEC_ARCH_FILTER}" ]; then
		        RPMS=`echo $RPMS | sed -e "s/[^ ]*debuginfo[^ ]*//g"`
	            else
		        RPMS=`echo $RPMS | sed -e "s/[^ ]*\(i686\|ppc\|s390\)\.rpm\|[^ ]*debuginfo[^ ]*//g"`
		    fi
		    for RPM in $RPMS; do
			rpmname=`echo $RPM | sed -e 's#.*/\(.*\)##'`
	                rlRun "$PKG_MGR -y reinstall $RPM" 0,1 "$PKG_MGR -y reinstall $rpmname"
		    done
		    rlLogInfo "$RPMS"
	            rlRun "$PKG_MGR -y downgrade $RPMS" 0,1 "$PKG_MGR -y downgrade"
		    rlRun "$PKG_MGR -y update $RPMS" 0,1 "$PKG_MGR -y update"
	            rlRun "$PKG_MGR -y install $RPMS" 0,1 "$PKG_MGR -y install"

	        else 
	            if echo $METHOD | grep -q local; then  # localinstall/localupdate
                        ARGS=$RPMLIST
                    else
                        ARGS=$PACKAGES
		    fi
	            rlRun -s "$PKG_MGR -y $METHOD $PARAMS $ARGS" 0,1

		    if [ $? -eq 1 ]; then
                        if echo $METHOD | egrep -q '(update|upgrade)' && egrep -q '(No packages marked for update|No packages marked for upgrade)' $rlRun_LOG; then
                            rlLogInfo "Ignoring unsuccessful $PKG_MGR $METHOD as no packages were originally installed"
                        else
                            rlFail "$PKG_MGR $METHOD failed, see the output above"
                        fi
                    fi
	        fi
		rlRun "check_pkg_versions_installed $RPMLIST" 
	        rpm -q $PACKAGES 
	    else
	        rlFail "No RPMs downloaded!!!"
	    fi
	else
	    rlFail "No builds passed!!!"
	fi
    rlPhaseEnd

if $VERSIONLOCK; then
    rlPhaseStartSetup "$PKG_MGR versionlock configuraion"
	rlRun "$PKG_MGR versionlock add $PNVRAS"
    rlPhaseEnd
fi

    rlPhaseStartCleanup
	popd
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
