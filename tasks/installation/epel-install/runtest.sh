#!/bin/sh

# Copyright (c) 2006 Red Hat, Inc. All rights reserved. This copyrighted material 
# is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General
# Public License v.2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Author: Alexander Todorov <atodorov@redhat.com>

# source the test script helpers 
. /usr/bin/rhts-environment.sh

echo "---------- start of runtest.sh ----------" | tee -a $OUTPUTFILE

# try to configure default EPEL urls
if [ -z "$TEST_PARAM_REPO_URL" ]; then
    case $FAMILY in
        RedHatEnterpriseLinux*5*)
            export TEST_PARAM_REPO_URL="http://download.fedora.redhat.com/pub/epel/5/#basearch/"
            ;;
        RedHatEnterpriseLinux4*)
            export TEST_PARAM_REPO_URL="http://download.fedora.redhat.com/pub/epel/4/#basearch/"
            ;;
    esac
fi

install_pkgs_case="rh-tests-distribution-install-install-packages-from-repo"
rpm -q "$install_pkgs_case"
res=$?

if [ $res -ne 0 ]; then
    yum -y install "$install_pkgs_case" 2>&1 | tee -a $OUTPUTFILE
fi

test_script=`rpm -ql "$install_pkgs_case" | grep helper.sh`

$test_script 2>&1 >> $OUTPUTFILE
res=$?

if [ $res -eq 0 ]; then
    report_result $TEST PASS
else
    report_result $TEST FAIL $res
fi

echo "---------- end of runtest.sh ----------" | tee -a $OUTPUTFILE
